﻿namespace BSA_ProjectStructure.Common
{
    public enum TaskStates
    {
        Created = 1,
        Started,
        Finished,
        Cancelled
    }
}
