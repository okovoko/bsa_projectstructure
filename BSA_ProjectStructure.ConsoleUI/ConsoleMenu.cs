﻿using System;
using System.Collections.Generic;
using BSA_ProjectStructure.Common;
using BSA_ProjectStructure.ConsoleUI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BSA_ProjectStructure.ConsoleUI
{
    class ConsoleMenu
    {
        private System.Net.Http.HttpClient client;
        private string baseURL;
        private JsonSerializerSettings _settings = new JsonSerializerSettings();
        bool exit;

        public ConsoleMenu()
        {
            client = new System.Net.Http.HttpClient();
            baseURL = @"https://localhost:44359/api/";
            this._settings.ContractResolver = new CustomContractResolver();
        }
        ~ConsoleMenu()
        {
            this.client.Dispose();
        }

        private void WriteLine(string text, ConsoleColor consoleColor = ConsoleColor.Yellow)
        {
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        public void Run()
        {
            while (!exit)
            {
                ShowMenuItems();
                int userInput = GetInput();
                Perform(userInput);
            }
        }

        private void Perform(int userInput)
        {
            switch (userInput)
            {
                case 1:
                    DisplayProjectUserTasksCount();
                    break;
                case 2:
                    DisplayUserTasks();
                    break;
                case 3:
                    DisplayFinishedTasks();
                    break;
                case 4:
                    DisplayAgeLimitTeams();
                    break;
                case 5:
                    DisplaySortedUsers();
                    break;
                case 6:
                    DisplayUserLastProjectInfo();
                    break;
                case 7:
                    DisplayProjectShortInfo();
                    break;
                case 8:
                    Exit();
                    break;

                default:
                    WriteLine("Please, Enter the number from 0 to 8.", ConsoleColor.Red);
                    break;
            }

            Console.WriteLine("Press enter");
            Console.ReadLine();
        }


        private int GetInput()
        {
            int userInput;
            do
            {
                WriteLine("Please, Enter your choice as a number:");
            } while (!int.TryParse(Console.ReadLine(), out userInput));

            return userInput;
        }

        private void ShowMenuItems()
        {
            Console.Clear();

            WriteLine("Choose option:");

            Console.WriteLine();

            WriteLine("1 - Display task count from the project of specific user");
            WriteLine("2 - Display tasks with description less than 45 symbols for the specific user");
            WriteLine("3 - Display finished tasks' id and name for the specific user (finishing year is 2019)");
            WriteLine("4 - Display teams collection with member older than 12 (Sorted by year of registration in descending order)");
            WriteLine("5 - Display User-Tasks (Sorted)");
            WriteLine("6 - Display User - Last user's project - Last project tasks' count - Unfinished tasks' count - The longest task");
            WriteLine("7 - Display Project - The longest project's task (by description) - The shortest project's task (by name) - Users' count ( Project description > 25 OR tasks' count < 3)");
            WriteLine("8 - Exit");

            Console.WriteLine();
        }

        private void DisplayProjectUserTasksCount()
        {
            WriteLine("Enter User id:");
            var input = GetInput();

            WriteLine("Answer:", ConsoleColor.Green);
            var data = JsonConvert.DeserializeObject<IEnumerable<ProjectUserTasksCount>>(client.GetStringAsync(baseURL + $@"users/{input}/projectUserTasksCount").Result, this._settings);
            foreach (var pair in data)
            {
                WriteLine($"Project id: {pair.Project.Id}\tProject name: {pair.Project.Name}\tTasks count: {pair.UserTasksCount}");
            }
        }

        private void DisplayUserTasks()
        {
            WriteLine("Enter User id:");
            var input = GetInput();

            WriteLine("Answer:", ConsoleColor.Green);
            var data = JsonConvert.DeserializeObject<IEnumerable<Task>>(client.GetStringAsync(baseURL + $@"users/{input}/tasks").Result, this._settings);
            foreach (var task in data)
            {
                WriteLine($"Task id: {task.Id}\tTask name: {task.Name}\nTasks state: {(TaskStates)task.StateId}");
                WriteLine($"Project id: {task.ProjectId}");
                WriteLine($"Created at: {task.CreatedAt.ToString()}\tFinished at: {task.FinishedAt.ToString()}\n");
            }
        }

        private void DisplayFinishedTasks()
        {
            WriteLine("Enter User id:");
            var input = GetInput();

            WriteLine("Answer:\nFinishing year is 2019", ConsoleColor.Green);
            var data = JsonConvert.DeserializeObject<IEnumerable<UserFinihedTasks>>(client.GetStringAsync(baseURL + $@"users/{input}/userFinihedTasks").Result, this._settings);
            foreach (var task in data)
            {
                WriteLine($"Task id: {task.Id}\tTask name: {task.Name}");
            }
        }

        private void DisplayAgeLimitTeams()
        {
            WriteLine("Answer:", ConsoleColor.Green);
            var data = JsonConvert.DeserializeObject<IEnumerable<AgeLimitTeam>>(client.GetStringAsync(baseURL + @"teams/ageLimitTeams").Result, this._settings);
            foreach (var team in data)
            {
                WriteLine($"Team id: {team.Id}\tTeam name: {team.Name}");
                foreach (var user in team.Users)
                {
                    WriteLine($"User id: {user.Id}\t User name: {user.FirstName} {user.LastName}\tRegistered at: {user.RegisteredAt}");
                }
                Console.WriteLine();
            }
        }

        private void DisplaySortedUsers()
        {
            WriteLine("Answer:", ConsoleColor.Green);
            var data = JsonConvert.DeserializeObject<IEnumerable<SortedUsers>>(client.GetStringAsync(baseURL + @"users/sortedUsers").Result, this._settings);
            foreach (var pair in data)
            {
                WriteLine($"User id: {pair.User.Id}\t User name: {pair.User.FirstName} {pair.User.LastName}");
                foreach (var task in pair.Tasks)
                {
                    WriteLine($"Task id: {task.Id}\tTask name: {task.Name}");
                }
                Console.WriteLine();
            }
        }

        private void DisplayUserLastProjectInfo()
        {
            WriteLine("Enter User id:");
            var input = GetInput();

            WriteLine("Answer:", ConsoleColor.Green);
            var data = JsonConvert.DeserializeObject<UserLastProjectInfo>(client.GetStringAsync(baseURL + $@"users/{input}/lastProject").Result, this._settings);
            WriteLine($"User id: {data.User.Id}\tUser name: {data.User.FirstName} {data.User.LastName}");
            WriteLine($"Last project id: {data.Project.Id} Name: {data.Project.Name}");
            WriteLine($"Tasks count: {data.TasksCount}\tUnfinished tasks coun: {data.UnfinishedTasksCount}");
            WriteLine($"The longest task: id: {data.LongestTask.Id} name: {data.LongestTask.Name}");
        }

        private void DisplayProjectShortInfo()
        {
            WriteLine("Enter Project id:");
            var input = GetInput();

            WriteLine("Answer:", ConsoleColor.Green);
            var data = JsonConvert.DeserializeObject<ProjectSummaryInfo>(client.GetStringAsync(baseURL + $@"projects/{input}/projectSummary").Result, this._settings);
            WriteLine($"Project id: {data.Project.Id} Name: {data.Project.Name}");
            WriteLine($"The longest project's task (by description):");
            if (data.LongestDescriptionTask == null)
            {
                WriteLine("There are no tasks", ConsoleColor.Red);
            }
            else
            {
                WriteLine($"Id: {data.LongestDescriptionTask.Id} Name: {data.LongestDescriptionTask.Name}");
            }
            WriteLine($"The shortest project's task (by name):");
            if (data.ShortestNameTask == null)
            {
                WriteLine("There are no tasks", ConsoleColor.Red);
            }
            else
            {
                WriteLine($"Id: {data.ShortestNameTask.Id} Name: {data.ShortestNameTask.Name}");
            }

            WriteLine($"Users' count ( Project description > 25 OR tasks' count < 3): {data.UserCount}");
        }

        private void Exit()
        {
            exit = true;
        }

        private class CustomContractResolver : DefaultContractResolver
        {
            private Dictionary<string, string> PropertyMappings { get; set; }

            public CustomContractResolver()
            {
                this.PropertyMappings = new Dictionary<string, string>
                {
                    {"Id", "id"},
                    {"Name", "name"},
                    {"Description", "description"},
                    {"CreatedAt", "createdAt"},
                    {"Deadline", "deadline"},
                    {"AuthorId", "authorId"},
                    {"TeamId", "teamId"},
                    {"FinishedAt", "finishedIt"},
                    {"StateId", "state"},
                    {"ProjectId", "projectId"},
                    {"PerformerId", "performerId"},
                    {"FirstName", "firstName"},
                    {"LastName", "lastName"},
                    {"Email", "email"},
                    {"Birthday", "birthday"},
                    {"RegisteredAt", "registeredAt"}
                };
            }

            protected override string ResolvePropertyName(string propertyName)
            {
                string resolvedName = null;
                var resolved = this.PropertyMappings.TryGetValue(propertyName, out resolvedName);
                return (resolved) ? resolvedName : base.ResolvePropertyName(propertyName);
            }
        }
    }
}
