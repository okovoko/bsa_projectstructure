﻿using System.Collections.Generic;

namespace BSA_ProjectStructure.ConsoleUI.Models
{
    public class AgeLimitTeam
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
