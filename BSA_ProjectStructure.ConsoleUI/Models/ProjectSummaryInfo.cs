﻿namespace BSA_ProjectStructure.ConsoleUI.Models
{
    public class ProjectSummaryInfo
    {
        public Project Project { get; set; }
        public Task LongestDescriptionTask { get; set; }
        public Task ShortestNameTask { get; set; }
        public int UserCount { get; set; }
    }
}
