﻿namespace BSA_ProjectStructure.ConsoleUI.Models
{
    public class ProjectUserTasksCount
    {
        public Project Project { get; set; }
        public int UserTasksCount { get; set; }
    }
}
