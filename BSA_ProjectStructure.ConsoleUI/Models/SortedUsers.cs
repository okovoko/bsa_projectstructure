﻿using System.Collections.Generic;

namespace BSA_ProjectStructure.ConsoleUI.Models
{
    public class SortedUsers
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
