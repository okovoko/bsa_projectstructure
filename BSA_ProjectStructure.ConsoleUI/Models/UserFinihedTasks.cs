﻿namespace BSA_ProjectStructure.ConsoleUI.Models
{
    public class UserFinihedTasks
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
