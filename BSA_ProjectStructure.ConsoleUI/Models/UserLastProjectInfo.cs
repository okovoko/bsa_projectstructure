﻿namespace BSA_ProjectStructure.ConsoleUI.Models
{
    public class UserLastProjectInfo
    {
        public User User { get; set; }
        public Project Project { get; set; }
        public int TasksCount { get; set; }
        public int UnfinishedTasksCount { get; set; }
        public Task LongestTask { get; set; }
    }
}
