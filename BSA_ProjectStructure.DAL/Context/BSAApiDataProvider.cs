﻿using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace BSA_ProjectStructure.DAL.Context
{
    public class BSAApiDataProvider : IDataProvider
    {
        private System.Net.Http.HttpClient _client;
        private readonly string _baseURL;
        private JsonSerializerSettings _settings = new JsonSerializerSettings();

        private List<User> _users;
        private List<Project> _projects;
        private List<Team> _teams;
        private List<Task> _tasks;

        public BSAApiDataProvider()
        {
            this._client = new System.Net.Http.HttpClient();
            this._baseURL = @"https://bsa2019.azurewebsites.net/";
            this._settings.ContractResolver = new CustomContractResolver();
        }
        ~BSAApiDataProvider()
        {
            this._client.Dispose();
        }

        public List<User> Users
        {
            get
            {
                if (this._users == null)
                {
                    this._users = JsonConvert.DeserializeObject<List<User>>
                        (this._client.GetStringAsync(this._baseURL + @"api/users").Result, this._settings);
                }
                return this._users;
            }
            private set { }
        }
        public List<Task> Tasks
        {
            get
            {
                if (this._tasks == null)
                {
                    this._tasks = JsonConvert.DeserializeObject<List<Task>>
                        (this._client.GetStringAsync(this._baseURL + @"api/tasks").Result, this._settings);
                }
                return this._tasks;
            }
            private set { }
        }
        public List<Project> Projects
        {
            get
            {
                if (this._projects == null)
                {
                    this._projects = JsonConvert.DeserializeObject<List<Project>>
                        (this._client.GetStringAsync(this._baseURL + @"api/projects").Result, this._settings);
                }
                return this._projects;
            }
            private set { }
        }
        public List<Team> Teams
        {
            get
            {
                if (this._teams == null)
                {
                    this._teams = JsonConvert.DeserializeObject<List<Team>>
                        (this._client.GetStringAsync(this._baseURL + @"api/teams").Result, this._settings);
                }
                return this._teams;
            }
            private set { }
        }

        private class CustomContractResolver : DefaultContractResolver
        {
            private Dictionary<string, string> PropertyMappings { get; set; }

            public CustomContractResolver()
            {
                this.PropertyMappings = new Dictionary<string, string>
                {
                    {"Id", "id"},
                    {"Name", "name"},
                    {"Description", "description"},
                    {"CreatedAt", "created_at"},
                    {"Deadline", "deadline"},
                    {"AuthorId", "author_id"},
                    {"TeamId", "team_id"},
                    {"FinishedAt", "finished_at"},
                    {"StateId", "state"},
                    {"ProjectId", "project_id"},
                    {"PerformerId", "performer_id"},
                    {"FirstName", "first_name"},
                    {"LastName", "last_name"},
                    {"Email", "email"},
                    {"Birthday", "birthday"},
                    {"RegisteredAt", "registered_at"}
                };
            }

            protected override string ResolvePropertyName(string propertyName)
            {
                string resolvedName = null;
                var resolved = this.PropertyMappings.TryGetValue(propertyName, out resolvedName);
                return (resolved) ? resolvedName : base.ResolvePropertyName(propertyName);
            }
        }
    }
}
