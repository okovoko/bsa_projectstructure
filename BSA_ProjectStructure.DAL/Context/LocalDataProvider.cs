using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BSA_ProjectStructure.DAL.Context
{
    public class LocalDataProvider : IDataProvider
    {
        private List<User> _users;
        private List<Project> _projects;
        private List<Team> _teams;
        private List<Task> _tasks;
        private DataGenerator _data = new DataGenerator();

        public List<User> Users
        {
            get
            {
                if (this._users == null)
                {
                    this._users = this._data.GenerateUsers(20);
                }
                return this._users;
            }
            private set { }
        }

        public List<Task> Tasks
        {
            get
            {
                if (this._tasks == null)
                {
                    this._tasks = this._data.GenerateTasks(20);
                }
                return this._tasks;
            }
            private set { }
        }

        public List<Project> Projects
        {
            get
            {
                if (this._projects == null)
                {
                    this._projects = this._data.GenerateProjects(20);
                }
                return this._projects;
            }
            private set { }
        }

        public List<Team> Teams
        {
            get
            {
                if (this._teams == null)
                {
                    this._teams = this._data.GenerateTeams(20);
                }
                return this._teams;
            }
            private set { }
        }

        private class DataGenerator
        {
            private System.Random random = new System.Random();

            public List<User> GenerateUsers(int number)
            {
                List<User> users = new List<User>();
                for (int i = 0; i < number; ++i)
                {
                    users.Add(new User()
                    {
                        Id = i + 1,

                        FirstName = RandomString(random.Next(5, 15)),
                        LastName = RandomString(random.Next(5, 15)),
                        Email = RandomString(random.Next(5, 30)) + "@test.net",

                        Birthday = System.DateTime.Now.AddYears(-random.Next(50)),
                        RegisteredAt = System.DateTime.Now.AddMonths(-random.Next(12))
                    });
                }
                return users;
            }

            public List<Task> GenerateTasks(int number)
            {
                List<Task> tasks = new List<Task>();
                for (int i = 0; i < number; ++i)
                {
                    tasks.Add(new Task()
                    {
                        Id = i + 1,

                        Name = RandomString(random.Next(25, 55)),
                        Description = RandomString(random.Next(25, 72)),

                        CreatedAt = System.DateTime.Now.AddMonths(-random.Next(3)),
                        FinishedAt = System.DateTime.Now.AddMonths(random.Next(3)),

                        PerformerId = random.Next(10),
                        ProjectId = random.Next(10),
                        StateId = random.Next(4)
                    });
                }
                return tasks;
            }

            public List<Team> GenerateTeams(int number)
            {
                List<Team> teams = new List<Team>();
                for (int i = 0; i < number; ++i)
                {
                    teams.Add(new Team()
                    {
                        Id = i + 1,

                        Name = RandomString(random.Next(5, 30)),

                        CreatedAt = System.DateTime.Now.AddMonths(-random.Next(6))
                    });
                }
                return teams;
            }

            public List<Project> GenerateProjects(int number)
            {
                List<Project> projects = new List<Project>();
                for (int i = 0; i < number; ++i)
                {
                    projects.Add(new Project()
                    {
                        Id = i + 1,

                        Name = RandomString(random.Next(5, 25)),
                        Description = RandomString(random.Next(15, 72)),

                        CreatedAt = System.DateTime.Now.AddMonths(-random.Next(6)),
                        Deadline = System.DateTime.Now.AddMonths(random.Next(6)),

                        AuthorId = random.Next(10),
                        TeamId = random.Next(10)
                    });
                }
                return projects;
            }

            public string RandomString(int length)
            {
                var characters = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz_0123456789";

                return new string(Enumerable.Repeat(characters, length).Select(s => s[random.Next(s.Length)]).ToArray());
            }
        }
    }
}
