﻿using BSA_ProjectStructure.DAL.Etities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA_ProjectStructure.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            // Task
            modelBuilder.Entity<Task>()
                .HasKey(t => t.Id);

            modelBuilder.Entity<Task>()
                .Property(t => t.Name)
                .HasMaxLength(72)
                .IsRequired();

            modelBuilder.Entity<Task>()
                .Property(t => t.Description)
                .HasMaxLength(255)
                .IsRequired(false);


            // Task's FKs
            modelBuilder.Entity<Task>()
                .HasOne(t => t.Project)
                .WithMany(p => p.Tasks)
                .HasForeignKey(t => t.ProjectId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Task>()
                .HasOne(t => t.Performer)
                .WithMany(u => u.Tasks)
                .HasForeignKey(t => t.PerformerId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Task>()
                .HasOne(t => t.State)
                .WithMany(ts => ts.Tasks)
                .HasForeignKey(t => t.StateId)
                .OnDelete(DeleteBehavior.SetNull);

            // TaskStates
            modelBuilder.Entity<TaskState>()
                .HasKey(ts => ts.Id);

            modelBuilder.Entity<TaskState>()
                .Property(ts => ts.Name)
                .HasMaxLength(15)
                .IsRequired();

            // TaskState's FKs
            modelBuilder.Entity<TaskState>()
                .HasMany(ts => ts.Tasks)
                .WithOne(t => t.State)
                .HasForeignKey(t => t.StateId)
                .OnDelete(DeleteBehavior.Restrict);

            //UserTeam
            modelBuilder.Entity<UserTeam>()
                .HasKey(ut => new { ut.UserId, ut.TeamId});

            modelBuilder.Entity<UserTeam>()
                .HasOne(ut => ut.User)
                .WithMany(u => u.UserTeams)
                .HasForeignKey(ut => ut.UserId);

            modelBuilder.Entity<UserTeam>()
                .HasOne(ut => ut.Team)
                .WithMany(t => t.UserTeams)
                .HasForeignKey(ut => ut.TeamId);

            // Team
            modelBuilder.Entity<Team>()
                .HasKey(t => t.Id);

            modelBuilder.Entity<Team>()
                .Property(t => t.Name)
                .HasMaxLength(30)
                .IsRequired();

            // Team's FKs
            modelBuilder.Entity<Team>()
                .HasMany(t => t.Projects)
                .WithOne(p => p.Team)
                .HasForeignKey(p => p.TeamId)
                .OnDelete(DeleteBehavior.SetNull);

            // Project
            modelBuilder.Entity<Project>()
                .HasKey(p => p.Id);

            modelBuilder.Entity<Project>()
                .Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(25);

            modelBuilder.Entity<Project>()
                .Property(p => p.Description)
                .IsRequired(false)
                .HasMaxLength(255);

            // Project's FKs

            modelBuilder.Entity<Project>()
                .HasMany(p => p.Tasks)
                .WithOne(t => t.Project)
                .HasForeignKey(p => p.ProjectId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Author)
                .WithMany(a => a.Projects)
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Team)
                .WithMany(t => t.Projects)
                .HasForeignKey(p => p.TeamId)
                .OnDelete(DeleteBehavior.SetNull);

            // User
            modelBuilder.Entity<User>()
                .HasKey(u => u.Id);

            modelBuilder.Entity<User>()
                .Property(u => u.FirstName)
                .HasMaxLength(15)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.LastName)
                .HasMaxLength(15)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.Email)
                .HasMaxLength(60)
                .IsRequired();

            // User's FKs
            modelBuilder.Entity<User>()
                .HasMany(u => u.Projects)
                .WithOne(p => p.Author)
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.Cascade);
            
            modelBuilder.Entity<User>()
                .HasMany(u => u.Tasks)
                .WithOne(t => t.Performer)
                .HasForeignKey(t => t.PerformerId)
                .OnDelete(DeleteBehavior.SetNull);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var random = new Random();
            var entityAmount = 20;


            var tasks = new List<Task>();
            for (int i = 0; i < entityAmount; ++i)
            {
                tasks.Add(new Task()
                {
                    Id = i + 1,

                    Name = RandomString(random.Next(25, 55)),
                    Description = RandomString(random.Next(25, 72)),

                    CreatedAt = DateTime.Now.AddMonths(-random.Next(3)),
                    FinishedAt = DateTime.Now.AddMonths(random.Next(3)),

                    PerformerId = random.Next(1, entityAmount),
                    ProjectId = random.Next(1, entityAmount),
                    StateId = random.Next(1, 4)
                });
            }

            var projects = new List<Project>();
            for (int i = 0; i < entityAmount; ++i)
            {
                projects.Add(new Project()
                {
                    Id = i + 1,

                    Name = RandomString(random.Next(5, 25)),
                    Description = RandomString(random.Next(15, 72)),

                    CreatedAt = DateTime.Now.AddMonths(-random.Next(6)),
                    Deadline = DateTime.Now.AddMonths(random.Next(6)),

                    AuthorId = random.Next(1, entityAmount),
                    TeamId = random.Next(1, entityAmount)
                });
            }

            var users = new List<User>();
            for (int i = 0; i < entityAmount; ++i)
            {
                var id = ++i;
                users.Add(new User()
                {
                    Id = id++,

                    FirstName = RandomString(random.Next(5, 15)),
                    LastName = RandomString(random.Next(5, 15)),
                    Email = RandomString(random.Next(5, 30)) + "@test.net",

                    Birthday = DateTime.Now.AddYears(-random.Next(50)),
                    RegisteredAt = DateTime.Now.AddMonths(-random.Next(12))
                });
            }

            var taskStates = new List<TaskState>();
            taskStates.Add(new TaskState()
            {
                Id = 1,
                Name = "Created"
            });
            taskStates.Add(new TaskState()
            {
                Id = 2,
                Name = "Started"
            });
            taskStates.Add(new TaskState()
            {
                Id = 3,
                Name = "Finished"
            });
            taskStates.Add(new TaskState()
            {
                Id = 4,
                Name = "Cancelled"
            });

            var teams = new List<Team>();
            for (int i = 0; i < entityAmount; ++i)
            {
                var id = ++i;
                teams.Add(new Team()
                {
                    Id = i++,

                    Name = RandomString(random.Next(5, 30)),

                    CreatedAt = DateTime.Now.AddMonths(-random.Next(6))
                });
            }

            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<TaskState>().HasData(taskStates);
            modelBuilder.Entity<Task>().HasData(tasks);
            modelBuilder.Entity<Team>().HasData(teams);

            string RandomString(int length)
            {
                var characters = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz_0123456789";

                return new string(Enumerable.Repeat(characters, length).Select(s => s[random.Next(s.Length)]).ToArray());
            }
        }
    }
}
