﻿using BSA_ProjectStructure.DAL.Etities;
using Microsoft.EntityFrameworkCore;

namespace BSA_ProjectStructure.DAL.Context
{
    public class TaskTrackerDbContext : DbContext
    {
        public TaskTrackerDbContext(DbContextOptions<TaskTrackerDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<User> Users { get; private set; }
        public DbSet<Task> Tasks { get; private set; }
        public DbSet<TaskState> TaskStates { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<Project> Projects { get; private set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();
            modelBuilder.Seed();
        }
    }
}
