﻿using System.Collections.Generic;

namespace BSA_ProjectStructure.DAL.Etities
{
    public class TaskState
    {
        public TaskState()
        {
            Tasks = new List<Task>();
        }

        public int Id { get; set; }

        public string Name { get; set; }
        public ICollection<Task> Tasks { get; private set; }
    }
}
