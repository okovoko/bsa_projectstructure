﻿using System;
using System.Collections.Generic;

namespace BSA_ProjectStructure.DAL.Etities
{
    public class Team
    {
        public Team()
        {
            UserTeams = new List<UserTeam>();
            Projects = new List<Project>();
        }

        public int Id { get; set; }

        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public ICollection<UserTeam> UserTeams { get; private set; }
        public ICollection<Project> Projects { get; internal set; }
    }
}
