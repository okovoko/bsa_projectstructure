﻿using System;
using System.Collections.Generic;

namespace BSA_ProjectStructure.DAL.Etities
{
    public class User
    {
        public User()
        {
            UserTeams = new List<UserTeam>();
            Tasks = new List<Task>();
            Projects = new List<Project>();
        }

        public int Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }

        public ICollection<UserTeam> UserTeams { get; internal set; }
        public ICollection<Task> Tasks { get; internal set; }
        public ICollection<Project> Projects { get; internal set; }
    }
}
