﻿using BSA_ProjectStructure.DAL.Etities;
using System.Collections.Generic;

namespace BSA_ProjectStructure.DAL.Interfaces
{
    public interface IDataProvider
    {
        List<User> Users { get; }
        List<Task> Tasks { get; }
        List<Project> Projects { get; }
        List<Team> Teams { get; }
    }
}
