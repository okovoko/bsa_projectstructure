﻿
using BSA_ProjectStructure.DAL.Etities;

namespace BSA_ProjectStructure.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Project> Projects { get; }
        IRepository<User> Users { get; }
        IRepository<Team> Teams { get; }
        IRepository<Task> Tasks { get; }

        void Save();
    }
}
