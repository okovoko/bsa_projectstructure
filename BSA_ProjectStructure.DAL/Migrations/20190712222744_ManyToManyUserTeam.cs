﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BSA_ProjectStructure.DAL.Migrations
{
    public partial class ManyToManyUserTeam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TaskStates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(maxLength: 15, nullable: false),
                    LastName = table.Column<string>(maxLength: 15, nullable: false),
                    Email = table.Column<string>(maxLength: 60, nullable: false),
                    Birthday = table.Column<DateTime>(nullable: false),
                    RegisteredAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 25, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Deadline = table.Column<DateTime>(nullable: false),
                    AuthorId = table.Column<int>(nullable: false),
                    TeamId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "UserTeam",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    TeamId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTeam", x => new { x.UserId, x.TeamId });
                    table.ForeignKey(
                        name: "FK_UserTeam_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserTeam_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 72, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    FinishedAt = table.Column<DateTime>(nullable: false),
                    StateId = table.Column<int>(nullable: false),
                    ProjectId = table.Column<int>(nullable: false),
                    PerformerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Users_PerformerId",
                        column: x => x.PerformerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tasks_TaskStates_StateId",
                        column: x => x.StateId,
                        principalTable: "TaskStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "TaskStates",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Created" },
                    { 2, "Started" },
                    { 3, "Finished" },
                    { 4, "Cancelled" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 20, new DateTime(2019, 6, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "f23Xbg92PvAFYCwoYv" },
                    { 19, new DateTime(2019, 3, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "AAPyLEUiqgF8xYoJXd7_" },
                    { 17, new DateTime(2019, 4, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "VkRRqp8hyvfmmfPJJ" },
                    { 16, new DateTime(2019, 5, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "hbRHw75Dy2I4BsBzmNuiUNGB" },
                    { 15, new DateTime(2019, 4, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "aLpABxXtEENmCOaXKDmTfP1" },
                    { 14, new DateTime(2019, 7, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "iQ4tNbUZRBRZvbpUCcZYX" },
                    { 13, new DateTime(2019, 3, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "2DVsLuelXGFHaVg_XvtjQ" },
                    { 12, new DateTime(2019, 2, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "BW_yOL" },
                    { 11, new DateTime(2019, 2, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "AXhFQ6K" },
                    { 18, new DateTime(2019, 5, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "zipLZYCGhdz1p2HJsrHLY6ZZ" },
                    { 9, new DateTime(2019, 3, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "CHHLAr_WlrvItsP7" },
                    { 10, new DateTime(2019, 3, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "3Nh49GvZ" },
                    { 2, new DateTime(2019, 3, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "wVtwZub1pDcjS2AMK1CH00NQz" },
                    { 3, new DateTime(2019, 7, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "Z2354w" },
                    { 4, new DateTime(2019, 4, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "IDSJuDJUSZcA3IJ33PU92hD4B" },
                    { 1, new DateTime(2019, 2, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "oT6R5wjcjeeEV2msyiVgxR" },
                    { 6, new DateTime(2019, 4, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "rG0Wwxfq" },
                    { 7, new DateTime(2019, 6, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "ancd6a9VhC0JBmUpqzQaFKCQnVmmf" },
                    { 8, new DateTime(2019, 7, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "wmTewQkEwiCKkiI" },
                    { 5, new DateTime(2019, 5, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "9TFFsoCXiXS" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[,]
                {
                    { 18, new DateTime(2008, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "XgBcWWr@test.net", "i31vb3cA2V7J", "lVetgrTILoz", new DateTime(2018, 10, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 11, new DateTime(1979, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "GBGHR9doF7B9p@test.net", "I9xCTrwtxk8Bas", "GKu90", new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 17, new DateTime(1997, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "sJRxeAEqi4bVapHuHLAKDlIOBO@test.net", "3PMRU_", "NGUsgJhb05sz_n", new DateTime(2018, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 16, new DateTime(1984, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "xy58tg1f9Xja1ukvpxv1@test.net", "EoA_feTFg", "0Q3HE", new DateTime(2018, 11, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 15, new DateTime(2011, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "Dtn1RH_KWqJbcrxXVpKnyH7c@test.net", "WbdbOkic8C", "8T1pCx", new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 14, new DateTime(2006, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "IMrOphrhfeduDC1Kh3m@test.net", "QhBu1", "au26Mr1R7p_", new DateTime(2018, 10, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 13, new DateTime(1977, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "cvVu5dMxen5WEwwAA04eU6SAr@test.net", "TjvhjD6xJM", "iOpcoM", new DateTime(2019, 4, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 12, new DateTime(2014, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "6BQY0hlK8xdEIn@test.net", "7kaDHCYxEGnQFP", "X3rC7r_brTes", new DateTime(2019, 3, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 10, new DateTime(1983, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "MYBw08kxXNchWa6s_0_dXFHBslzl@test.net", "olsnoXVXynV", "4ksSvGBe", new DateTime(2018, 11, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 5, new DateTime(2017, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "T83SJo7P9rOPeEq7GMTTXHRCdv53@test.net", "9UF1a3NAbsh", "swfvB2gc", new DateTime(2018, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 8, new DateTime(1981, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "uYus93CMU3DcZzkcT29Z78Cwj_4@test.net", "hDQjXdgESfJ", "PiEqL5Oyi", new DateTime(2018, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 7, new DateTime(2017, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "9g6qFLgZDbJjO@test.net", "YinAgnzE", "mc6lWD60YB40m", new DateTime(2018, 11, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 6, new DateTime(2003, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "OsRbNDVcXPx2v4UGegFpiJhkh@test.net", "LxuMuIiXNoQbB", "_nsUX_psPgMn", new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 4, new DateTime(1991, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "KOiUcX5Y@test.net", "dBg5KakxdF", "bmQaXi", new DateTime(2018, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 3, new DateTime(2009, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "g85iuk0LZcJXdv2d@test.net", "9RP7hbsDTpnRaC", "7J1xM2Sa", new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 2, new DateTime(1973, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "85jT7l0ijBJisUEfZEFtPx6MkJB9@test.net", "1791v7xGn5flGb", "Lw0bLsr5A6GAxu", new DateTime(2019, 1, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 1, new DateTime(2019, 7, 13, 1, 27, 37, 642, DateTimeKind.Local).AddTicks(450), "Ge8gT@test.net", "bMCTpjpGG3tFS", "CRzK0e99BoBSjr", new DateTime(2018, 10, 13, 1, 27, 37, 646, DateTimeKind.Local).AddTicks(452) },
                    { 19, new DateTime(2005, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "sJNh2@test.net", "aG88s", "ukVban", new DateTime(2019, 1, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 9, new DateTime(2004, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "jfIKrZo6VW5Je8jnd@test.net", "s6mknXuEEK", "fqMMm", new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) },
                    { 20, new DateTime(1987, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "V6IcsFMu4cXFWu8q@test.net", "NBSjvb4f8mb3Lu", "uH8oTAyYcjgnZJ", new DateTime(2018, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 2, 1, new DateTime(2019, 2, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 12, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "xFicTxuOAH60d6l141faog4GJg54", "mbX9hu6kcEpVNbvk2", 9 },
                    { 1, 8, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 12, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "TI36tAzP6fhy56GF8wUpj55aW20qMknsk5WI", "as2qhDHK5", 3 },
                    { 17, 7, new DateTime(2019, 5, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 9, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "kaHSRlaLJQEtJI3HAgHhA9R14iOzuo6CEfClYRrDWYYHawTj_Nf4p9wk", "wdCXFL3SwUGEwh", 3 },
                    { 5, 7, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 10, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "9i_UlmIcLACmAvNgVvRKFZyMXqZ7Y1wocdQDA2", "JXGOEF", 5 },
                    { 20, 6, new DateTime(2019, 2, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "GGuSwR0RobjL6MN1yhvXhhziSXOOKdgHZ_W6h_Wxa6epEnTyr5qn6dt6Ml98cMgL", "uW3e3Nnj1ptrnyRF8yjVU1", 1 },
                    { 15, 6, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "T9nH8cvAMPs9pyP3loAUE_8deT9o9QEeSi4csdt", "risUO_Q", 2 },
                    { 13, 6, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 12, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "smX6Ha0tDCSoVIwbaXbeYJc1Dvukm94Ra8uBNsEQcf", "cN5fw", 5 },
                    { 6, 6, new DateTime(2019, 5, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "qYs_hfpn30sJjpJsj3FKGyM2rvxziBrnDhK62I2ksS9Ob", "7dW8l1", 6 },
                    { 18, 5, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 9, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "arEow3m2JFQC7u6HpdK7wIlOodeqpsZ", "GvzrCPDGQOOudqhgK0fVE8h", 5 },
                    { 19, 4, new DateTime(2019, 5, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "uirYGV9TX1lWlJt", "LUoTbSL7aFIu36F", 8 },
                    { 12, 4, new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "3Svme5_0ogQWFJ9BYzbgNxBK1gdQ4KgDQegrDsDSTaADc0LpN_XXnIneqlq6zj6", "1PKTrQym2Qrpo", 2 },
                    { 11, 3, new DateTime(2019, 3, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 12, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "_YcruiHl5nw45y0YT8AdE", "iUCdEE6t_20xUj9dvFm", 5 },
                    { 16, 2, new DateTime(2019, 5, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "i8z1JYaKcLDHa2kONH1mJI0wMru7S5AGuJheKW8EbiaIfaRJuCyl4m", "qgTyZ", 1 },
                    { 14, 2, new DateTime(2019, 4, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 9, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "WsvUnTRCScgjr6wbMswNoILwRXseS8cWqCBksl6S", "6iaMfErWjj10RLQgdeB", 2 },
                    { 10, 2, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 11, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "sH51yBvVvmdOpdmVDEhcNzeLgpZjX", "w812985FtJqJ7genmZyjuG", 5 },
                    { 3, 2, new DateTime(2019, 4, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "rjRezF0YqhcZMzX", "tBDkjO9kyuWmh", 1 },
                    { 9, 1, new DateTime(2019, 3, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 10, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "tnvo1lc476uIinIMGxm8FP9JVCCNWWLnKNUH", "mA5l6U6zRIhLnapiv6", 9 },
                    { 4, 1, new DateTime(2019, 2, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "LsLwgxS0Q0IQbfTIqQl_qVbBRm", "P3IQsNfVVRGZb", 9 },
                    { 8, 8, new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "Z6UFSyRCW5ZgVY5ZNZ_6KN1XGU", "5NdF3iDMKzKblzW6O2fBHQm", 3 },
                    { 7, 9, new DateTime(2019, 4, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "GCFkwba_wtZ5zcQGexuPHR4S", "MCGRKxMWirnVe1i", 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[,]
                {
                    { 4, new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "gRp04OaMSOg05VdQxSRUNouY9WGV7d2bZ0LPjILiXv1IIfvFDw8", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "x2hP4TNqOkKhek1_imp3lhaKB3g_cHPmSBzDIXYcnWPBt_ghwGfV", 1, 2, 3 },
                    { 16, new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "dO5sW0uB3LsBurEU4vlw_lYKsMPZ7eBBDUSBka1iiqVLvlCc4PT7WmdXvs9KL0cXKr6A8", new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "E1CvvdHsWpItSd5rSwimXUbdquCE", 9, 8, 1 },
                    { 20, new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "hIWlYe47G60IXPwCUnV4IjKQgZN", new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "Vzj1jIjBee22tloQAkAmYmc_Eht1lH2rRDFVDw2dM0F", 8, 1, 3 },
                    { 9, new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "cQLP6WoS2arfw8tiSpXsugVZ9InkVdCHePqdwX4aPNMuj0bKnTGsnCF1_rPviE4", new DateTime(2019, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "PRuPlVaBXMr2FN_jTUcg1X6Aiv1ayjb2YDfV", 3, 1, 1 },
                    { 3, new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "Dv2CIXBopnl6Fh1Q5iQtFm0UE3y6D40kpcGxsFmiEhZNdvrOv7r9pHvv0H5l6cLss", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "3Hqb4RwJDRjAF6qy3wWeYtppSmDLk6JnPhuaFwj1pUXWrPpIa0", 1, 1, 1 },
                    { 5, new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "NEPhbFgyGKjwjlvBtcCHpogwlVmfcWu1", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "X1GhLnorebCjtgPHI3wTmpZaBrHEINxWtsSmFBMIMvddT0oQPMl", 3, 5, 1 },
                    { 15, new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "Y9v84vab4SyLc6QTz9QDSbwihIU3qIpu", new DateTime(2019, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "VYuTdcdIsWDBrcxTva6ynyHmdOV", 5, 6, 1 },
                    { 13, new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "1wHZkVZ6koWLjnTmkIaYvB1n2dWr4w0Ut7tlzPxdf2a3x6cYX1AC_eG_0Z5H7HBttC", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "g4F4WnULzYqmH3fQ44FCfpR0PcBJDy9_llojMGLILk15", 8, 3, 2 },
                    { 11, new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "0_OGAbK49Nd8lQGONoRT0aQRfE2NoyvxbYzG73bmfGI3U16N6iqDL", new DateTime(2019, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "_jaO_rfXIuuMuBNcCOpDhKx_K6Ew7S4T", 8, 3, 3 },
                    { 1, new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "xdlf743UnotIf503l2Rpn1NabrEMh3DulC7AIDc2L5T5H", new DateTime(2019, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "YA4z34UIpAoTd3QwdtibyIrbM10cddYrQOQ", 2, 3, 3 },
                    { 18, new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "L2UmGIwb1PPJ946OUBg71wr0WAAmX", new DateTime(2019, 9, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "ZsxGR9arTtVj9V7DIZwpFjjxisqlQMVZsnzIchM9iL0k", 5, 9, 2 },
                    { 7, new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "uaatxU83nzSuR3_TJiI1D9xKkMQwfQv5AnFY46w7y8nfPIzRpkntDIuVivil", new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "EhsjEGzZ6rosx229b5LOK7w4ld", 7, 9, 1 },
                    { 8, new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "W0GZFB20Bz2KhroVgPr2Bp9O0eYG8a", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "UDXbvkrjcERr_cr7g_CC_xyPpHVk_qhQC0tWzOUcbZFQ", 3, 4, 3 },
                    { 2, new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "lWoXLCbj48Vd_QCrhvYgmH50h1PuGr", new DateTime(2019, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "t4AA3vLdmYQmqL83d80UiyueQhpyPu6", 8, 4, 1 },
                    { 19, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "niMcIE2tNcJKtYkg76sRMDuwi6iSLdBefx3w_PNSGMW3jGdEKXh", new DateTime(2019, 9, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "w27KE6XX7k7DkOqtjtMHUKyvfhKKUuaKK9HSvwcAd8TQX4V7Gsp_", 3, 2, 2 },
                    { 14, new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "AwSj9xU7Tv9vi9KySG15l8U3RKEq2pNm9f6Yv_Sf4nGGdj5acGK", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "Q4sZA7V5a6_bwYZPYZQARqF60miVHx7pIEr5RMz2dAGam840iH", 3, 2, 1 },
                    { 12, new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "sMbA92C2GhiXbFi7LW6yAvgRDCW9WvvNI7E8", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "1F3OGwl4sW9PqmenuYT4jfoPf", 7, 2, 2 },
                    { 10, new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "76bzbkpfH_1Pq65yglSl7YWfjQKMg8ARoCdlsod4wSdVYgO22DwvytxfH1", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "pR_swdHzApeGD3C4oZCjuJFhgxkQtW8zz35ClA", 1, 2, 3 },
                    { 17, new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "A77TGhHhPeXr1iFa_xLQZhiCJz6WXWycYIq7o0XZ3fS7cT00O68J4O7YV2Kl", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "JvmUHjM3oRCKvq_LXaR5NO6BdnbdzfwEt7zBi7", 5, 8, 2 },
                    { 6, new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "cm3Nek81eAuzIR37kSLO8gy4Rm6Wdf7QSuhxxqYN3pTaBVm2yxwtr0x1t91", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "L9zTbCMMzhFn2mKeU64x1lYBvpVnVWCG4j", 3, 7, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_AuthorId",
                table: "Projects",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_PerformerId",
                table: "Tasks",
                column: "PerformerId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_StateId",
                table: "Tasks",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTeam_TeamId",
                table: "UserTeam",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "UserTeam");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "TaskStates");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
