﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BSA_ProjectStructure.DAL.Migrations
{
    public partial class ProjectCascadeDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { new DateTime(2019, 2, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "QXj7ZAdqnq6gBDTH1WTVHJZ1miBX9URYkYFq7wML6Tasfq7IOmOlCctg5nRq8Mqy", "TOdtViIVGLbeodEXALCtKCVY" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2019, 4, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 11, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "0MRcHTAuvKMdjBzkA6wxugGD5", "CieKIXTigv2jYisGFWIE6TQ", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "BTVHtO3o3d34d93YQUfOetyRbqK4lKS6FP3DcnSxgOWvS", "9ox8o1f", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "v5c8lpKkM2xhNDpi7IPtHLeet6M4", "O_vXMmzdFDd8DrR7xUcyvcbn", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 8, new DateTime(2019, 4, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "yWKqspXzrZAkY6kNdmRcp895NARZo3zstoL4T1jirRgOPkWyzlPMc", "pvPXeZsZtw", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2019, 6, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "hcIQ7YqncJx9cNNQ8JRGHXwKtGRocTmgMSHjzEgBf6BJG6Ihr3XAaHSQl62x", "ds75Lu9mwjSN", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 8, new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "9w52hoZDSSmVkXBnG26pGJZ6", "qsQGdv0CQaiWndv6j4f2MG", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "sTW4vEaOkGIP99Ov7v49bbu26m93K3grhlikyiTV4l0tVvfsbuP_rfxR6_8JE", "fSpknqPf", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 12, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "MAbtXM7jjqYHNu9OS16YZXjDbk4tWCXjoASVf98", "HTwD3VchcvGBgIgm_u", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, new DateTime(2019, 5, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 10, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "yoYmCjis3H_n7sYsRqm3ngNrM59H", "KOyMHzUq23tUv7rmokT", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "zSbGaeKbFK8HVnV44FHPkMSxpGh3BOwjVBwkZplFsp2jV0QhmIbvWOln", "q50vMj4RBtuCfvcf7065", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "masHuJkQZRIFPeW9J1qCeU3URQ4XGBE36hKrCK8N9sThZwlLRcLzomTxE6", "NAwsVRUwkdmyb30QIcuv9P", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 9, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "rBu0zerfDVjnqtrnEu0", "xNLgh_e472D", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2019, 5, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 10, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "vtIWyJBBvif_NjaBy5ilCgr07Bz0h8CYWpzpIWMD3KtYqtVq7YtiZX1obmXxPfZwgNE5", "npdqmfm5lKbwoc09zNgcd", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 9, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "LO17UTWQ4rDpxBnARxthd2ICmMgJklnoeJEC4d3ryHvHH_THOOZ", "fPxsyKKLi3oky78", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 10, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "n0Co2Ga0SY8_2SmEnYu2", "GyrhmmfqEUko3L", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, new DateTime(2019, 4, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "UiF58onSJ7XQ4vhP71f4czB5IDtUX", "beDsPeKcLlfI", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 12, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "GlcrOcTMpALFzhhV20Z_yxQUXS1yYJAfib0RfqFREEWgpAJEoqvk588n0n", "dlWGSfwZ", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 4, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "XjhsVIjCmWXwiKjppgkdqxyaLUviKm", "g9F3YMvpkvhvuBjEX", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 8, new DateTime(2019, 5, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 10, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "RDZtX8qgharBXAfOUu0yd22", "QlTK4Sv88xtm4M", 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "vv9zfAH6bO8lHY63SIHTFCsdVw4n6oTTiaJta4", new DateTime(2019, 9, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "xI4jebMCv7wN9gFMnZRuj8KaX3zy4TLTXIjwn8G5M", 6, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "7cIIzbPVwmiX5DmM0YwESqFOTJzyw4iviHrTHB1jFxKiUx8rdx4p9tMN4ZuQhv6zdSoWRZ", new DateTime(2019, 9, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "tlaqyqm6vydtc2eODy9s_lDgPj1VRNBGQvPjvlUNbV4P4VqexIwY", 9, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "RLpBvXxSBhkl3FE8J5VnseCn_vzfonsG1VCgFQQiHgFxv", new DateTime(2019, 9, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "G3z1Khu7AyWU0EvNMLboZQebJiAjsDTSVRga9yG9B8zk2z0NdX", 2, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "qLuDrcRDvb_bYcbDom8K8giEpXrM6Mn9VP2BRiN", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "POj_MJbG95SdTuepgD4ZymMGl8KXHyUnT2Dxr_V5d", 2, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "r8aVsWa7Y7Yf_ZW3figVofl8ug8oVjOZ", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "RdAWOlI7ZKjCQszqFFjt_ligArG8XE1Evq6TRvPAFa2", 6, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "2NHPcor9inInPp9jRT3HZccsjgf9tUdS6zE", new DateTime(2019, 8, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "MBD3lXxYyx9s_3zgvMsbaMwRJop7vWeVlu3frrzU", 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "lsy5GxHy3BqrAdsEQ5DRIPT9Fa7IBl8sCf5h33QKchfWDsumZGd5yZO28y", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "7mI2h3e41A74NNZktf1JrltKvPulA_Pm3J_VzWCKacxUeuqmkt02k", 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "PQAkOSC4fZhq5IadgcecEpCIkQUIWoRL91Yqz5JoGG6qiSSmtb3XBlz9nX955zGwI3qbPp", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "C5PzGLhtmyOAj32SlRO_IBzIXBIDEkQDpuUfD_GO", 8, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "6KiTXNKp3806eFkfKWL0GnCZZdQgwQHpg85dtEPSlVuFTUY_", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "RR9Y5htUSZjtBycC5LqM9giMBQ4Exs6LP2n3xg9vfReoS4AWmHlne", 9, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "m0Fk1vI2iNI3ssEVwnj_YXliUIHIJYf5_P_lM0lcHUpo5IxCWz3EDC4V", new DateTime(2019, 9, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "JQIoZt3DylxLXFhHRfHYPqy3GFLV0PTrN3", 7, 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "DUfpJQT2ZWiTlChUxf10s8_87jZWJ60vjY6kCD4EvCwKZI", new DateTime(2019, 9, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "sQHKbDYYJ1bHTEH3klpeRP1b_YSe2pFytHb9hsGUj6QBC1oynsRjcj", 2, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "QkeWo_4zR1S5myo2Pg7PSile2pKK2", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "Ho0EZGq9w3s00SJTE4SUIXeqhpfWg", 8, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "cQZI4VCqf7IQibcBcEjVJF5cMd0Kfq60fv2g6iKi6", new DateTime(2019, 9, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "IvEdOvjlWCkKLbla7XXqH7A_7Qs5h0xeZNgf", 5, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "TYA3O2yBRG72UyCi_LoV6HH1mJSRbh0z4Yn3b4Gb2sM", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "ZuDWi4QB5SW1Z_xLuamkSf2DzzaX9llx8ebVCzu6uU6UHmQnG6Eq5C", 5, 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "S3N4NM6vLQKBL4nBxuTccEqAK6OlAbTDRxrSXMZTluyORJzJPduEXxWOLmsP5S", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "VJFoMnMRePFjH9jLE82kFg5eNyk_", 9, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "7SLYvy4n1vrHt7sQj5n1qEg4gHcDftYxa6lG84", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "L0jl09wNLCOaEe23MoLZBdHkyKVy8PRKZg6UzJJXBic", 6, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "hjNVcipghMmGu4ZfR_RkUpx0gGzpkhjhaJs5kFieChoN0BI3mpBr9bfxRrO3O91", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "UWa0WyNd0Hx_lnroWzIh4zgvsMqm", 2, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "VUAsYZwgri7q3aYK2gIoQbVFvg9r9JjBkrFrxVfJdeHnnFFLvA", new DateTime(2019, 8, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "2_MRMiMaKAF65c_gxI7D0lyWMWCMokW13S", 6, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "8zpPnsTuxVywDlqFJhYjp7zjTunNpMN1ygRgEMR2Bxo1Y2zMc", new DateTime(2019, 8, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "7_3rzFHI84Gk1gSgB8LL_hXQBU18PumHqrs4YTP757O", 7, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "BKfwXVUGqg6QxX0sDt176ryix9J2iqbVE7D18UpHiVjDcM5eNeMKuY2PR38Xlx5A", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "aAG1yeGFlh452_ja4v2IXP6qX", 4, 8, 2 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "iwhY2f0NPO1p9FljkR68ZJ9xC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "ugMc3" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "5QyZerex8fykZ7QSC93Z98X" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "iFfGcL8JIH0qRMMzbKZ7XZ" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "UjD9mw7y" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "1m8R2zz" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 2, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "wuBqsQ86U5q8PDarYI" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "qiChOq90QO9EvhKpsypPNyj0xdl" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "9DR0eaOSZcCqEnG6mihYq79YPOX8" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "Kev4qLnJqH25yWN7J" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "4L0EqNa" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 2, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "LviPLjx50H4_pDXa3sFikqFm" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "EPAFPphU6_6m" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "6pdbnImhDdUz1_Oz" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "Tbfi4VXms0F4CPBvk_ND7s33l3" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "UNGdjCyQjuSnKaVrcGbey" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 3, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "TMN4aei4aebYfuBs9" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 2, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "MVI9y65kzf0l3o5SX" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "xLaxriII" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 2, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "BsbnIyzLmU8DzowfYFJhcjw_6el" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1972, 7, 13, 1, 31, 56, 513, DateTimeKind.Local).AddTicks(8516), "cE4tDYrkMrDxRVETnYCjTBz@test.net", "gQtTpUVwfv5X", "nbBMBbtbEV4", new DateTime(2019, 5, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2000, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "_L4ATlsseBj7@test.net", "7HKvc_", "1NpYDC4lmI27", new DateTime(2018, 8, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "fzWCDaMrmWIeci@test.net", "kAvPIBvIU0kve", "gy696iTKKcWhdw", new DateTime(2019, 4, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2010, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "HRJVdQbezBZoyDz1Ik3aW7tvk@test.net", "Ou6Il7OwgAv", "Ya483Oattb4", new DateTime(2019, 5, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2008, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "3dI4TK6K@test.net", "EUBMXdB_uYsv7", "ywgOfytFli", new DateTime(2019, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1975, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "o39mG8IYH_AY5bOulwQTXzwqWU7@test.net", "9mwuC9", "PxGCFF", new DateTime(2019, 1, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1986, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "OvRAlhiAC@test.net", "_6D9en64BB4HF2", "jbeBc_kuRfU", new DateTime(2018, 8, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2003, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "aq922rUEhBMMuf_V9@test.net", "AWOvoWpu_2mSz0", "dpiRzQiPg", new DateTime(2019, 1, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1972, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "fQY7jYOrzbX_cweQ@test.net", "7GqZeQZ", "DMwgHG", new DateTime(2018, 8, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1970, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "M61YJJVtfKBsRhNwp2SMLC@test.net", "q_AfwLYQa0jFeO", "uegLIQQtwxNV", new DateTime(2019, 4, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1974, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "o_FxWic@test.net", "umaGJnjvbE", "8R20Y", new DateTime(2019, 5, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2000, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "07E5iCrP@test.net", "_JLK7rFt2uN33", "4PdkX79XThsF", new DateTime(2018, 11, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1985, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "ezkIQyjHjSxxprahwAw8fDz@test.net", "dNlh8dvS5N_HF", "fI2DVdXyNKVx", new DateTime(2019, 3, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "WRWK7yRBAU2ZGh3jqfR@test.net", "SCKAbSgMqxsF", "O0Q1Vdkf", new DateTime(2019, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "wtn0px6iQhst@test.net", "URviduh", "rBqaLy", new DateTime(2019, 6, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2007, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "QQvIfaqBVAt@test.net", "yCRO4eUVu2Z1c0", "F2DAZrd", new DateTime(2019, 2, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2008, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "OuE0d0osZv8H0wTKJzT__56ZQdV@test.net", "42ec6y7W3", "nhcwQrkMVza5g", new DateTime(2019, 1, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1999, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "wKKAfHBFHArrckvFHS@test.net", "O8uitiUdSBm", "nMXJwTYLqgKMb", new DateTime(2018, 8, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1982, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "XweqnuL8@test.net", "RCgmbogDh", "igDrKyS0fzi", new DateTime(2018, 8, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1993, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "9YLluwHFgMkph@test.net", "BFhDT", "Yzb9kJ", new DateTime(2019, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 12, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "TI36tAzP6fhy56GF8wUpj55aW20qMknsk5WI", "as2qhDHK5" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 2, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 12, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "xFicTxuOAH60d6l141faog4GJg54", "mbX9hu6kcEpVNbvk2", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, new DateTime(2019, 4, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "rjRezF0YqhcZMzX", "tBDkjO9kyuWmh", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 2, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "LsLwgxS0Q0IQbfTIqQl_qVbBRm", "P3IQsNfVVRGZb", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 10, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "9i_UlmIcLACmAvNgVvRKFZyMXqZ7Y1wocdQDA2", "JXGOEF", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2019, 5, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "qYs_hfpn30sJjpJsj3FKGyM2rvxziBrnDhK62I2ksS9Ob", "7dW8l1", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 9, new DateTime(2019, 4, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "GCFkwba_wtZ5zcQGexuPHR4S", "MCGRKxMWirnVe1i", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 8, new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "Z6UFSyRCW5ZgVY5ZNZ_6KN1XGU", "5NdF3iDMKzKblzW6O2fBHQm", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 3, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 10, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "tnvo1lc476uIinIMGxm8FP9JVCCNWWLnKNUH", "mA5l6U6zRIhLnapiv6", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 11, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "sH51yBvVvmdOpdmVDEhcNzeLgpZjX", "w812985FtJqJ7genmZyjuG", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, new DateTime(2019, 3, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 12, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "_YcruiHl5nw45y0YT8AdE", "iUCdEE6t_20xUj9dvFm", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "3Svme5_0ogQWFJ9BYzbgNxBK1gdQ4KgDQegrDsDSTaADc0LpN_XXnIneqlq6zj6", "1PKTrQym2Qrpo", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 12, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "smX6Ha0tDCSoVIwbaXbeYJc1Dvukm94Ra8uBNsEQcf", "cN5fw", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, new DateTime(2019, 4, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 9, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "WsvUnTRCScgjr6wbMswNoILwRXseS8cWqCBksl6S", "6iaMfErWjj10RLQgdeB", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "T9nH8cvAMPs9pyP3loAUE_8deT9o9QEeSi4csdt", "risUO_Q", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, new DateTime(2019, 5, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "i8z1JYaKcLDHa2kONH1mJI0wMru7S5AGuJheKW8EbiaIfaRJuCyl4m", "qgTyZ", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2019, 5, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 9, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "kaHSRlaLJQEtJI3HAgHhA9R14iOzuo6CEfClYRrDWYYHawTj_Nf4p9wk", "wdCXFL3SwUGEwh", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 9, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "arEow3m2JFQC7u6HpdK7wIlOodeqpsZ", "GvzrCPDGQOOudqhgK0fVE8h", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2019, 5, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "uirYGV9TX1lWlJt", "LUoTbSL7aFIu36F", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2019, 2, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "GGuSwR0RobjL6MN1yhvXhhziSXOOKdgHZ_W6h_Wxa6epEnTyr5qn6dt6Ml98cMgL", "uW3e3Nnj1ptrnyRF8yjVU1", 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "xdlf743UnotIf503l2Rpn1NabrEMh3DulC7AIDc2L5T5H", new DateTime(2019, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "YA4z34UIpAoTd3QwdtibyIrbM10cddYrQOQ", 2, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "lWoXLCbj48Vd_QCrhvYgmH50h1PuGr", new DateTime(2019, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "t4AA3vLdmYQmqL83d80UiyueQhpyPu6", 8, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "Dv2CIXBopnl6Fh1Q5iQtFm0UE3y6D40kpcGxsFmiEhZNdvrOv7r9pHvv0H5l6cLss", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "3Hqb4RwJDRjAF6qy3wWeYtppSmDLk6JnPhuaFwj1pUXWrPpIa0", 1, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "gRp04OaMSOg05VdQxSRUNouY9WGV7d2bZ0LPjILiXv1IIfvFDw8", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "x2hP4TNqOkKhek1_imp3lhaKB3g_cHPmSBzDIXYcnWPBt_ghwGfV", 1, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "NEPhbFgyGKjwjlvBtcCHpogwlVmfcWu1", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "X1GhLnorebCjtgPHI3wTmpZaBrHEINxWtsSmFBMIMvddT0oQPMl", 3, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "cm3Nek81eAuzIR37kSLO8gy4Rm6Wdf7QSuhxxqYN3pTaBVm2yxwtr0x1t91", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "L9zTbCMMzhFn2mKeU64x1lYBvpVnVWCG4j", 3, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "uaatxU83nzSuR3_TJiI1D9xKkMQwfQv5AnFY46w7y8nfPIzRpkntDIuVivil", new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "EhsjEGzZ6rosx229b5LOK7w4ld", 7, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "W0GZFB20Bz2KhroVgPr2Bp9O0eYG8a", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "UDXbvkrjcERr_cr7g_CC_xyPpHVk_qhQC0tWzOUcbZFQ", 3, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "cQLP6WoS2arfw8tiSpXsugVZ9InkVdCHePqdwX4aPNMuj0bKnTGsnCF1_rPviE4", new DateTime(2019, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "PRuPlVaBXMr2FN_jTUcg1X6Aiv1ayjb2YDfV", 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "76bzbkpfH_1Pq65yglSl7YWfjQKMg8ARoCdlsod4wSdVYgO22DwvytxfH1", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "pR_swdHzApeGD3C4oZCjuJFhgxkQtW8zz35ClA", 1, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "0_OGAbK49Nd8lQGONoRT0aQRfE2NoyvxbYzG73bmfGI3U16N6iqDL", new DateTime(2019, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "_jaO_rfXIuuMuBNcCOpDhKx_K6Ew7S4T", 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "sMbA92C2GhiXbFi7LW6yAvgRDCW9WvvNI7E8", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "1F3OGwl4sW9PqmenuYT4jfoPf", 7, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "1wHZkVZ6koWLjnTmkIaYvB1n2dWr4w0Ut7tlzPxdf2a3x6cYX1AC_eG_0Z5H7HBttC", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "g4F4WnULzYqmH3fQ44FCfpR0PcBJDy9_llojMGLILk15", 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "AwSj9xU7Tv9vi9KySG15l8U3RKEq2pNm9f6Yv_Sf4nGGdj5acGK", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "Q4sZA7V5a6_bwYZPYZQARqF60miVHx7pIEr5RMz2dAGam840iH", 3, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "Y9v84vab4SyLc6QTz9QDSbwihIU3qIpu", new DateTime(2019, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "VYuTdcdIsWDBrcxTva6ynyHmdOV", 5, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "dO5sW0uB3LsBurEU4vlw_lYKsMPZ7eBBDUSBka1iiqVLvlCc4PT7WmdXvs9KL0cXKr6A8", new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "E1CvvdHsWpItSd5rSwimXUbdquCE", 9, 8, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "A77TGhHhPeXr1iFa_xLQZhiCJz6WXWycYIq7o0XZ3fS7cT00O68J4O7YV2Kl", new DateTime(2019, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "JvmUHjM3oRCKvq_LXaR5NO6BdnbdzfwEt7zBi7", 5, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "L2UmGIwb1PPJ946OUBg71wr0WAAmX", new DateTime(2019, 9, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "ZsxGR9arTtVj9V7DIZwpFjjxisqlQMVZsnzIchM9iL0k", 5, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "niMcIE2tNcJKtYkg76sRMDuwi6iSLdBefx3w_PNSGMW3jGdEKXh", new DateTime(2019, 9, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "w27KE6XX7k7DkOqtjtMHUKyvfhKKUuaKK9HSvwcAd8TQX4V7Gsp_", 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "hIWlYe47G60IXPwCUnV4IjKQgZN", new DateTime(2019, 8, 13, 1, 27, 37, 648, DateTimeKind.Local).AddTicks(453), "Vzj1jIjBee22tloQAkAmYmc_Eht1lH2rRDFVDw2dM0F", 8, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 2, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "oT6R5wjcjeeEV2msyiVgxR" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 3, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "wVtwZub1pDcjS2AMK1CH00NQz" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "Z2354w" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "IDSJuDJUSZcA3IJ33PU92hD4B" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "9TFFsoCXiXS" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "rG0Wwxfq" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "ancd6a9VhC0JBmUpqzQaFKCQnVmmf" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "wmTewQkEwiCKkiI" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 3, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "CHHLAr_WlrvItsP7" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 3, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "3Nh49GvZ" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 2, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "AXhFQ6K" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 2, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "BW_yOL" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 3, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "2DVsLuelXGFHaVg_XvtjQ" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "iQ4tNbUZRBRZvbpUCcZYX" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "aLpABxXtEENmCOaXKDmTfP1" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "hbRHw75Dy2I4BsBzmNuiUNGB" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "VkRRqp8hyvfmmfPJJ" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "zipLZYCGhdz1p2HJsrHLY6ZZ" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 3, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "AAPyLEUiqgF8xYoJXd7_" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 27, 37, 649, DateTimeKind.Local).AddTicks(454), "f23Xbg92PvAFYCwoYv" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 27, 37, 642, DateTimeKind.Local).AddTicks(450), "Ge8gT@test.net", "bMCTpjpGG3tFS", "CRzK0e99BoBSjr", new DateTime(2018, 10, 13, 1, 27, 37, 646, DateTimeKind.Local).AddTicks(452) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1973, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "85jT7l0ijBJisUEfZEFtPx6MkJB9@test.net", "1791v7xGn5flGb", "Lw0bLsr5A6GAxu", new DateTime(2019, 1, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2009, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "g85iuk0LZcJXdv2d@test.net", "9RP7hbsDTpnRaC", "7J1xM2Sa", new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "KOiUcX5Y@test.net", "dBg5KakxdF", "bmQaXi", new DateTime(2018, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2017, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "T83SJo7P9rOPeEq7GMTTXHRCdv53@test.net", "9UF1a3NAbsh", "swfvB2gc", new DateTime(2018, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2003, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "OsRbNDVcXPx2v4UGegFpiJhkh@test.net", "LxuMuIiXNoQbB", "_nsUX_psPgMn", new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2017, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "9g6qFLgZDbJjO@test.net", "YinAgnzE", "mc6lWD60YB40m", new DateTime(2018, 11, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1981, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "uYus93CMU3DcZzkcT29Z78Cwj_4@test.net", "hDQjXdgESfJ", "PiEqL5Oyi", new DateTime(2018, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2004, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "jfIKrZo6VW5Je8jnd@test.net", "s6mknXuEEK", "fqMMm", new DateTime(2019, 6, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1983, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "MYBw08kxXNchWa6s_0_dXFHBslzl@test.net", "olsnoXVXynV", "4ksSvGBe", new DateTime(2018, 11, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1979, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "GBGHR9doF7B9p@test.net", "I9xCTrwtxk8Bas", "GKu90", new DateTime(2019, 5, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "6BQY0hlK8xdEIn@test.net", "7kaDHCYxEGnQFP", "X3rC7r_brTes", new DateTime(2019, 3, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1977, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "cvVu5dMxen5WEwwAA04eU6SAr@test.net", "TjvhjD6xJM", "iOpcoM", new DateTime(2019, 4, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2006, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "IMrOphrhfeduDC1Kh3m@test.net", "QhBu1", "au26Mr1R7p_", new DateTime(2018, 10, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "Dtn1RH_KWqJbcrxXVpKnyH7c@test.net", "WbdbOkic8C", "8T1pCx", new DateTime(2019, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1984, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "xy58tg1f9Xja1ukvpxv1@test.net", "EoA_feTFg", "0Q3HE", new DateTime(2018, 11, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1997, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "sJRxeAEqi4bVapHuHLAKDlIOBO@test.net", "3PMRU_", "NGUsgJhb05sz_n", new DateTime(2018, 9, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2008, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "XgBcWWr@test.net", "i31vb3cA2V7J", "lVetgrTILoz", new DateTime(2018, 10, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2005, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "sJNh2@test.net", "aG88s", "ukVban", new DateTime(2019, 1, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1987, 7, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453), "V6IcsFMu4cXFWu8q@test.net", "NBSjvb4f8mb3Lu", "uH8oTAyYcjgnZJ", new DateTime(2018, 8, 13, 1, 27, 37, 647, DateTimeKind.Local).AddTicks(453) });

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
