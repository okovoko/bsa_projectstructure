﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BSA_ProjectStructure.DAL.Migrations
{
    public partial class FixDataGenerator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 10, new DateTime(2019, 5, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 11, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "qucBwm9uVmueYT2UgOa225EbOemvYSP", "Kz1KBGuHqEQgSaiYXpb", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 17, new DateTime(2019, 7, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 10, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "sqBSr9j3D5jnimH5M_lV3lAScWnREaG2qCsdvh", "w_TUG9RdolO8EtRiPF", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 9, new DateTime(2019, 3, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 11, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "OP1rxxCXn4gzDhEZwyy5ZNFq1Un1xYBRlsHxfcELloXA6", "GF5Fr4WFnDpQH", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 11, new DateTime(2019, 7, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 11, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "AlTtuNRkVFSd_7_DMKzC2fn0maBePCEJjjOuaXbjxlL", "lWXJwBKKvdjg8u2uO", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { new DateTime(2019, 4, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 11, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "Mm0sBJzqQFCUIQKpziDzXvYTzSO5VtwIQN5kQYEOYx_HZTJKbEA", "M1IR0Mt_5YH", 17 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 17, new DateTime(2019, 3, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 10, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "91RzGJd1RtCSDqHwi8", "tHyO0302nzE0s88beU", 18 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { new DateTime(2019, 5, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 7, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "LhvCobfD3x_w0lYmVRrxtmt1Mp10sWNx", "y8POXce6QaL3_cNsFwajIlHa", 16 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 13, new DateTime(2019, 6, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 11, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "ViV6tzv8fKpGkcIW965Zpss_PJiQFQsvdTcfOngUfCZ_Jw9pTtLK8Yjcv0oKq", "1B8FZ5KS2KLI3S", 15 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 13, new DateTime(2019, 7, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 10, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "JApT73DAiEDPtIPqlysVE2J5jWaxIhPocXn", "cwAChrwJtNyxqxjy4RFiw", 12 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 7, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 12, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "xautM3_P7XfYPqXUihS8O3CDybQaJAKL6_1", "GTeUubdBJlfMVBgNWrjh2", 13 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2019, 4, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 10, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "Jbu3407r0kMzG6mz_Uu", "GzNng4rvV", 12 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 13, new DateTime(2019, 5, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 12, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "B5AenFk_MtoBMC5abNaoJdilKV80Ro8gddyWUogchqMAAspZrZiZWZ", "ytPt32P_kgwi", 13 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 12, new DateTime(2019, 4, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 11, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "FJXfFQjW0BAto3YhoLYvgpknv", "xGPi5uh4iJglsG5HYwILw", 12 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 10, new DateTime(2019, 5, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 11, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "srOFL8XBoqI4sB7u6jprXXcUuXQ21SWw2BcUbfCW1XdD6PVCTlhHh491", "MiDSBfRXP", 12 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 9, new DateTime(2019, 6, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 10, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "T9WFbOiPKoo7Bge", "0F3_Rar7KsYHyW" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2019, 3, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 8, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "dJYo9oG8hpcLrASVrLCbQ", "rHIMo", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2019, 6, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 10, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "hn3V3jmsMdnLg6QR1yYutvs7v3cJOxQeZ4JhqwzAJL61ipIbhQd1NuIR058AHDEtT", "qvnMwKW6K9w1o9Yb8ufj", 13 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2019, 5, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 10, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "ab6UUXeuXMsxZyGgjhA66m8p83VJ8zdIYlgbG88RMu52mMF8ebn", "AE8xLiXwVG", 14 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 15, new DateTime(2019, 5, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 11, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "RJPDcsXve4MUsqVS8XjHoR8AsCmwmABDGGvME8lc9", "COVXC0V5ik8nSeaKi", 11 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 13, new DateTime(2019, 7, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), new DateTime(2019, 8, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "2iOM0JOAlHhc_Ho7K77m_Yc", "Xnx9FiaB9AKPpAfAJ", 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 3, 53, 57, 728, DateTimeKind.Local).AddTicks(2372), "h_U_Dh4i7vYVXEelZHfZCLKBWn62CaSJ65iO6DrzyDHRBzpQBySo5GZ2psdVJBg5DP", new DateTime(2019, 9, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "un6X4hQoRr1fVsbHmJM6zQJS5VCWMGhzPHUfelWD9", 17, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "Ad8vPXcwYzIC_knZIZP6SJyrJtHXXFVR8znnD9FTheax5Y5", new DateTime(2019, 7, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "VLA7EY926n_JODLxEM6k83qx3JnrcvUkk2ZSf_xwsHw0g4", 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "V4pexgn_Ioniy1EVxV3Q3CCF3AuJyZI0nN", new DateTime(2019, 9, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "83WKNM0QL8TewcA3AvIt2WniBtQW_sjWtzA5XDvTKD3fNT", 7, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "D5DLuxKLDh6AfZ6b5iIW8QW0czMD", new DateTime(2019, 8, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "f8XoJz3XGNEpQcKs0KhxREbogS0lakkKdmK4kT1", 6, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "WRaFjaHozOPh4oxvCUoNg3jCbNym7cB2SzVYdEehxsqWTkjyqeJSEiYzh6d3hzQwmTuDy8", new DateTime(2019, 9, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "UtavZOIPiyMKEjoi9bkeDsghi0lgTv", 10, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "1kb4nm4gHXRrKoA39kgNl0w3N4vToX9l0LELecDfO1u1T3jaM2IsPqratFpqWJxpxFW7K", new DateTime(2019, 9, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "LToWD92ZyoxjB1qFaI95CZJAvGARSLc3seOtPmyI6W6", 18, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "zMAx_Kh8PoDanYxJ7DTtOC7eXFzFpZLbv3xu_ABuvbsv4k0TN48", new DateTime(2019, 9, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "Qf1iQsVgsoM2KZdw9srPedFDOGkvXELlOrB_Y286acealilFG5YjV", 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "Tr4s0ajSkOasYDWIeUULiK31mWVXA1p7", new DateTime(2019, 9, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "1aVpCPvDbKqaco6b66moiHMKgxszWkuyAH79l", 7, 16, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "yfTu4CeQPHAOC6GXfTZgBsbCvHL593AH4JvxO", new DateTime(2019, 7, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "lLAUNeZDSrcXCKLa2iVbGeLrro6b48x2WPIikU4xbjEdu", 14, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "zPLZwDBf9g7MSdwGaj11zX0CWxzlVedAfgGKn0VItsp", new DateTime(2019, 8, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "BI3S8RKdqjOhfEq8VWHqsgYqYInJ6qDP0TazyuMluYP4Obpt4IM9Pu", 13, 18 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "JFsYOMmUdvexW77cUtRORTZjGaBRk0PWBEAp3CUrZXZWTN5tJesqasivLJrNL0JI", new DateTime(2019, 8, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "ClVtySctic7JVp0c6Gl4MxFUshHheSb", 14, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "o2Isyj6sDAhXhXUjSbxYvlITqCxXNhhmuQln2aX4ilkQhnOTaql7ZIhcTUCWrGF1Y0cXL", new DateTime(2019, 8, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "fS89UzZDWyDmIGI7vO0KeTQHg7nqpfWku_7aRa", 11, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "NPI9phVobMfTJPNdr97x323ZBUtU", new DateTime(2019, 7, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "KBXlcTEW31ismIJzh6akCGoxmkwIzDzt8Y5B2i8", 18, 19, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 3, 53, 57, 733, DateTimeKind.Local).AddTicks(2375), "Yl9PqmRtdM8vS95ptMEQeq0ESdqOXsBMs3Sf0WrKhORjPGIKHZ4M5Eshq13m", new DateTime(2019, 7, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "5S6PIFVhGn4gkPkZo2jHfRso70TBEzOHULr_fcXZCuvo2_jejqYbU", 17, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "3I8gpRV27_WdUMbcuduLDe6SxjiJXYvCLSmcVYjTyisfIYnf1mp14xYxjMoGilEGe", new DateTime(2019, 9, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "DvmXU5zzIW8RUM0gH5VUoOJku9ZeOeRa73cS5PF9rd", 2, 19, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "6fRCpkf7z5jOFhkfdrWwFvFwv8P2QKtYK", new DateTime(2019, 9, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "2zbSzrYK81cNKI_FszuYijDKmEMBUAMG3Atb0", 9, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "TOm_i409zefBCY9x8JNqI2j_2MqiUbwd5kxNrCoAXXRr", new DateTime(2019, 8, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "YenKOwf_E1W2bLdAIa_Mq1JOpjRExl173x5SXlw9pEkaZnSNg", 11, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "XANj7hsTODhfai8dSTfMyEXkKqO0ROs3TMmrqSNK", new DateTime(2019, 7, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "OiAVeXeS73MrLxNCdXaSG4zmyq", 19, 19, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "0zJOsAqAD8nQ8MnygIsxQ4nPTg3tcJywcDYbEdMfjTqGk7y", new DateTime(2019, 9, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "AjgyhyTeRsiC9NyXb9QLrt1NS9f0ttFyoYqeUxGZi", 17, 11, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "X0QrE_VKiAcmvwATq4qKgocyCCReuf8L9Wb", new DateTime(2019, 7, 13, 3, 53, 57, 734, DateTimeKind.Local).AddTicks(2375), "Q75lZ0MD45Zfwn2TMuc1NO3MHeuNx4", 17, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 7, 13, 3, 53, 57, 736, DateTimeKind.Local).AddTicks(2376), "KAZ54r0676GXVQDIHELAOFPVFT9z3" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 7, 13, 3, 53, 57, 736, DateTimeKind.Local).AddTicks(2376), "gzxTkRz_gS1ckfHLRCA" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 5, 13, 3, 53, 57, 736, DateTimeKind.Local).AddTicks(2376), "mrQeQEYjSabZM8543Yw" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 2, 13, 3, 53, 57, 736, DateTimeKind.Local).AddTicks(2376), "enScEP5InX2D" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 13, 3, 53, 57, 736, DateTimeKind.Local).AddTicks(2376), "hHO_NbqtRzi7GjnUjJUJId5Cl" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 5, 13, 3, 53, 57, 736, DateTimeKind.Local).AddTicks(2376), "beJFWmfVVO6jLYQ3YzfitzZns" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 6, 13, 3, 53, 57, 736, DateTimeKind.Local).AddTicks(2376), "qEjMOYHPPIuSraUxGl9iqx" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2002, 7, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376), "VgB2jdGJGFHyqAKIaI@test.net", "oL38Cr2Ifks", "RF4yZ3fca", new DateTime(2019, 5, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2007, 7, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376), "R25Xzxv0RaUh362v@test.net", "9lp1V1JkjP8U", "Vv5fIYb3enhxt3", new DateTime(2019, 6, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2006, 7, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376), "_F90MFtQF6@test.net", "ktK_ASe3_6I", "B70yNr", new DateTime(2018, 11, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2018, 7, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376), "lC97rGipamarLUCeJE6DI@test.net", "TarzHb5eV114i4", "JbFodajnky5", new DateTime(2019, 5, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2009, 7, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376), "WAmS2asDP6Fe@test.net", "yZt7JS", "BjMRCv4", new DateTime(2018, 9, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1970, 7, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376), "En9GP@test.net", "mL02FYSjGnG9I", "cAol_Hc0nkCfvE", new DateTime(2019, 5, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1997, 7, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376), "pZ9cnibAgyy84YuUKcMtJVB_RXc@test.net", "ZXQEWgE", "BkEM6vYlTXr", new DateTime(2018, 11, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1988, 7, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376), "EbQk5@test.net", "sxnIwAxnWNe", "ohrePF", new DateTime(2018, 10, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1989, 7, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376), "mRRCmpU3hs3AHqZ1BqT0DlhFdMOo@test.net", "eqvGTksGaRUb", "22M7PFgyiMI", new DateTime(2018, 8, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1981, 7, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376), "bfiIw@test.net", "EnIgybdtB9FNJ", "Mw6LLXXyNzBJl7", new DateTime(2019, 1, 13, 3, 53, 57, 735, DateTimeKind.Local).AddTicks(2376) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2019, 5, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 10, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "vtIWyJBBvif_NjaBy5ilCgr07Bz0h8CYWpzpIWMD3KtYqtVq7YtiZX1obmXxPfZwgNE5", "npdqmfm5lKbwoc09zNgcd", 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "7cIIzbPVwmiX5DmM0YwESqFOTJzyw4iviHrTHB1jFxKiUx8rdx4p9tMN4ZuQhv6zdSoWRZ", new DateTime(2019, 9, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "tlaqyqm6vydtc2eODy9s_lDgPj1VRNBGQvPjvlUNbV4P4VqexIwY", 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "2NHPcor9inInPp9jRT3HZccsjgf9tUdS6zE", new DateTime(2019, 8, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "MBD3lXxYyx9s_3zgvMsbaMwRJop7vWeVlu3frrzU", 1, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "6KiTXNKp3806eFkfKWL0GnCZZdQgwQHpg85dtEPSlVuFTUY_", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "RR9Y5htUSZjtBycC5LqM9giMBQ4Exs6LP2n3xg9vfReoS4AWmHlne", 9, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "m0Fk1vI2iNI3ssEVwnj_YXliUIHIJYf5_P_lM0lcHUpo5IxCWz3EDC4V", new DateTime(2019, 9, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "JQIoZt3DylxLXFhHRfHYPqy3GFLV0PTrN3", 7, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "cQZI4VCqf7IQibcBcEjVJF5cMd0Kfq60fv2g6iKi6", new DateTime(2019, 9, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "IvEdOvjlWCkKLbla7XXqH7A_7Qs5h0xeZNgf", 5, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "TYA3O2yBRG72UyCi_LoV6HH1mJSRbh0z4Yn3b4Gb2sM", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "ZuDWi4QB5SW1Z_xLuamkSf2DzzaX9llx8ebVCzu6uU6UHmQnG6Eq5C", 5, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "S3N4NM6vLQKBL4nBxuTccEqAK6OlAbTDRxrSXMZTluyORJzJPduEXxWOLmsP5S", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "VJFoMnMRePFjH9jLE82kFg5eNyk_", 9, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "8zpPnsTuxVywDlqFJhYjp7zjTunNpMN1ygRgEMR2Bxo1Y2zMc", new DateTime(2019, 8, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "7_3rzFHI84Gk1gSgB8LL_hXQBU18PumHqrs4YTP757O", 7, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "iwhY2f0NPO1p9FljkR68ZJ9xC" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "iFfGcL8JIH0qRMMzbKZ7XZ" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 2, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "wuBqsQ86U5q8PDarYI" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "Kev4qLnJqH25yWN7J" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 4, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "EPAFPphU6_6m" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "UNGdjCyQjuSnKaVrcGbey" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "xLaxriII" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 5, new DateTime(2019, 6, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "UjD9mw7y" },
                    { 8, new DateTime(2019, 6, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "qiChOq90QO9EvhKpsypPNyj0xdl" },
                    { 9, new DateTime(2019, 6, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "9DR0eaOSZcCqEnG6mihYq79YPOX8" },
                    { 3, new DateTime(2019, 5, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "5QyZerex8fykZ7QSC93Z98X" },
                    { 11, new DateTime(2019, 7, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "4L0EqNa" },
                    { 12, new DateTime(2019, 2, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "LviPLjx50H4_pDXa3sFikqFm" },
                    { 2, new DateTime(2019, 6, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "ugMc3" },
                    { 14, new DateTime(2019, 4, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "6pdbnImhDdUz1_Oz" },
                    { 15, new DateTime(2019, 4, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "Tbfi4VXms0F4CPBvk_ND7s33l3" },
                    { 6, new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "1m8R2zz" },
                    { 17, new DateTime(2019, 3, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "TMN4aei4aebYfuBs9" },
                    { 18, new DateTime(2019, 2, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "MVI9y65kzf0l3o5SX" },
                    { 20, new DateTime(2019, 2, 13, 1, 31, 56, 520, DateTimeKind.Local).AddTicks(8520), "BsbnIyzLmU8DzowfYFJhcjw_6el" }
                });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1972, 7, 13, 1, 31, 56, 513, DateTimeKind.Local).AddTicks(8516), "cE4tDYrkMrDxRVETnYCjTBz@test.net", "gQtTpUVwfv5X", "nbBMBbtbEV4", new DateTime(2019, 5, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "fzWCDaMrmWIeci@test.net", "kAvPIBvIU0kve", "gy696iTKKcWhdw", new DateTime(2019, 4, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2008, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "3dI4TK6K@test.net", "EUBMXdB_uYsv7", "ywgOfytFli", new DateTime(2019, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1986, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "OvRAlhiAC@test.net", "_6D9en64BB4HF2", "jbeBc_kuRfU", new DateTime(2018, 8, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1972, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "fQY7jYOrzbX_cweQ@test.net", "7GqZeQZ", "DMwgHG", new DateTime(2018, 8, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1974, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "o_FxWic@test.net", "umaGJnjvbE", "8R20Y", new DateTime(2019, 5, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1985, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "ezkIQyjHjSxxprahwAw8fDz@test.net", "dNlh8dvS5N_HF", "fI2DVdXyNKVx", new DateTime(2019, 3, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "wtn0px6iQhst@test.net", "URviduh", "rBqaLy", new DateTime(2019, 6, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2008, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "OuE0d0osZv8H0wTKJzT__56ZQdV@test.net", "42ec6y7W3", "nhcwQrkMVza5g", new DateTime(2019, 1, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1982, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "XweqnuL8@test.net", "RCgmbogDh", "igDrKyS0fzi", new DateTime(2018, 8, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[,]
                {
                    { 2, new DateTime(2000, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "_L4ATlsseBj7@test.net", "7HKvc_", "1NpYDC4lmI27", new DateTime(2018, 8, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) },
                    { 12, new DateTime(2000, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "07E5iCrP@test.net", "_JLK7rFt2uN33", "4PdkX79XThsF", new DateTime(2018, 11, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) },
                    { 8, new DateTime(2003, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "aq922rUEhBMMuf_V9@test.net", "AWOvoWpu_2mSz0", "dpiRzQiPg", new DateTime(2019, 1, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) },
                    { 14, new DateTime(2014, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "WRWK7yRBAU2ZGh3jqfR@test.net", "SCKAbSgMqxsF", "O0Q1Vdkf", new DateTime(2019, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) },
                    { 6, new DateTime(1975, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "o39mG8IYH_AY5bOulwQTXzwqWU7@test.net", "9mwuC9", "PxGCFF", new DateTime(2019, 1, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) },
                    { 16, new DateTime(2007, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "QQvIfaqBVAt@test.net", "yCRO4eUVu2Z1c0", "F2DAZrd", new DateTime(2019, 2, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) },
                    { 4, new DateTime(2010, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "HRJVdQbezBZoyDz1Ik3aW7tvk@test.net", "Ou6Il7OwgAv", "Ya483Oattb4", new DateTime(2019, 5, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) },
                    { 18, new DateTime(1999, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "wKKAfHBFHArrckvFHS@test.net", "O8uitiUdSBm", "nMXJwTYLqgKMb", new DateTime(2018, 8, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) },
                    { 10, new DateTime(1970, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "M61YJJVtfKBsRhNwp2SMLC@test.net", "q_AfwLYQa0jFeO", "uegLIQQtwxNV", new DateTime(2019, 4, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) },
                    { 20, new DateTime(1993, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "9YLluwHFgMkph@test.net", "BFhDT", "Yzb9kJ", new DateTime(2019, 7, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518) }
                });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 8, new DateTime(2019, 2, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "QXj7ZAdqnq6gBDTH1WTVHJZ1miBX9URYkYFq7wML6Tasfq7IOmOlCctg5nRq8Mqy", "TOdtViIVGLbeodEXALCtKCVY", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2019, 4, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 11, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "0MRcHTAuvKMdjBzkA6wxugGD5", "CieKIXTigv2jYisGFWIE6TQ", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "BTVHtO3o3d34d93YQUfOetyRbqK4lKS6FP3DcnSxgOWvS", "9ox8o1f", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "v5c8lpKkM2xhNDpi7IPtHLeet6M4", "O_vXMmzdFDd8DrR7xUcyvcbn", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { new DateTime(2019, 4, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "yWKqspXzrZAkY6kNdmRcp895NARZo3zstoL4T1jirRgOPkWyzlPMc", "pvPXeZsZtw", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2019, 6, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "hcIQ7YqncJx9cNNQ8JRGHXwKtGRocTmgMSHjzEgBf6BJG6Ihr3XAaHSQl62x", "ds75Lu9mwjSN", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "9w52hoZDSSmVkXBnG26pGJZ6", "qsQGdv0CQaiWndv6j4f2MG", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "sTW4vEaOkGIP99Ov7v49bbu26m93K3grhlikyiTV4l0tVvfsbuP_rfxR6_8JE", "fSpknqPf", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 12, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "MAbtXM7jjqYHNu9OS16YZXjDbk4tWCXjoASVf98", "HTwD3VchcvGBgIgm_u", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, new DateTime(2019, 5, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 10, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "yoYmCjis3H_n7sYsRqm3ngNrM59H", "KOyMHzUq23tUv7rmokT", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "zSbGaeKbFK8HVnV44FHPkMSxpGh3BOwjVBwkZplFsp2jV0QhmIbvWOln", "q50vMj4RBtuCfvcf7065", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "masHuJkQZRIFPeW9J1qCeU3URQ4XGBE36hKrCK8N9sThZwlLRcLzomTxE6", "NAwsVRUwkdmyb30QIcuv9P", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 9, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "rBu0zerfDVjnqtrnEu0", "xNLgh_e472D", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 7, new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 9, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "LO17UTWQ4rDpxBnARxthd2ICmMgJklnoeJEC4d3ryHvHH_THOOZ", "fPxsyKKLi3oky78" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2019, 7, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 10, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "n0Co2Ga0SY8_2SmEnYu2", "GyrhmmfqEUko3L", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, new DateTime(2019, 4, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "UiF58onSJ7XQ4vhP71f4czB5IDtUX", "beDsPeKcLlfI", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, new DateTime(2019, 3, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 12, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "GlcrOcTMpALFzhhV20Z_yxQUXS1yYJAfib0RfqFREEWgpAJEoqvk588n0n", "dlWGSfwZ", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2019, 4, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 8, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "XjhsVIjCmWXwiKjppgkdqxyaLUviKm", "g9F3YMvpkvhvuBjEX", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 8, new DateTime(2019, 5, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), new DateTime(2019, 10, 13, 1, 31, 56, 519, DateTimeKind.Local).AddTicks(8519), "RDZtX8qgharBXAfOUu0yd22", "QlTK4Sv88xtm4M", 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "vv9zfAH6bO8lHY63SIHTFCsdVw4n6oTTiaJta4", new DateTime(2019, 9, 13, 1, 31, 56, 517, DateTimeKind.Local).AddTicks(8518), "xI4jebMCv7wN9gFMnZRuj8KaX3zy4TLTXIjwn8G5M", 6, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "RLpBvXxSBhkl3FE8J5VnseCn_vzfonsG1VCgFQQiHgFxv", new DateTime(2019, 9, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "G3z1Khu7AyWU0EvNMLboZQebJiAjsDTSVRga9yG9B8zk2z0NdX", 2, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "qLuDrcRDvb_bYcbDom8K8giEpXrM6Mn9VP2BRiN", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "POj_MJbG95SdTuepgD4ZymMGl8KXHyUnT2Dxr_V5d", 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 6, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "r8aVsWa7Y7Yf_ZW3figVofl8ug8oVjOZ", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "RdAWOlI7ZKjCQszqFFjt_ligArG8XE1Evq6TRvPAFa2", 6, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "lsy5GxHy3BqrAdsEQ5DRIPT9Fa7IBl8sCf5h33QKchfWDsumZGd5yZO28y", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "7mI2h3e41A74NNZktf1JrltKvPulA_Pm3J_VzWCKacxUeuqmkt02k", 8, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "PQAkOSC4fZhq5IadgcecEpCIkQUIWoRL91Yqz5JoGG6qiSSmtb3XBlz9nX955zGwI3qbPp", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "C5PzGLhtmyOAj32SlRO_IBzIXBIDEkQDpuUfD_GO", 8, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "DUfpJQT2ZWiTlChUxf10s8_87jZWJ60vjY6kCD4EvCwKZI", new DateTime(2019, 9, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "sQHKbDYYJ1bHTEH3klpeRP1b_YSe2pFytHb9hsGUj6QBC1oynsRjcj", 2, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "QkeWo_4zR1S5myo2Pg7PSile2pKK2", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "Ho0EZGq9w3s00SJTE4SUIXeqhpfWg", 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "7SLYvy4n1vrHt7sQj5n1qEg4gHcDftYxa6lG84", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "L0jl09wNLCOaEe23MoLZBdHkyKVy8PRKZg6UzJJXBic", 6, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "hjNVcipghMmGu4ZfR_RkUpx0gGzpkhjhaJs5kFieChoN0BI3mpBr9bfxRrO3O91", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "UWa0WyNd0Hx_lnroWzIh4zgvsMqm", 2, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 5, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "VUAsYZwgri7q3aYK2gIoQbVFvg9r9JjBkrFrxVfJdeHnnFFLvA", new DateTime(2019, 8, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "2_MRMiMaKAF65c_gxI7D0lyWMWCMokW13S", 6, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[] { new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "BKfwXVUGqg6QxX0sDt176ryix9J2iqbVE7D18UpHiVjDcM5eNeMKuY2PR38Xlx5A", new DateTime(2019, 7, 13, 1, 31, 56, 518, DateTimeKind.Local).AddTicks(8519), "aAG1yeGFlh452_ja4v2IXP6qX", 4, 8, 2 });
        }
    }
}
