﻿using BSA_ProjectStructure.DAL.Context;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BSA_ProjectStructure.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly TaskTrackerDbContext _data;

        public ProjectRepository(TaskTrackerDbContext data)
        {
            this._data = data;
        }

        public void Create(Project item)
        {
            this._data.Projects.Add(item);
        }

        public void Delete(int id)
        {
            if (this._data.Projects.Any(p => p.Id == id))
            {
                this._data.Projects.Remove(this._data.Projects.Find(id));
            }
        }

        public Project Get(int id)
        {
            return this._data.Projects.FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<Project> GetAll()
        {
            return this._data.Projects;
        }

        public void Update(Project item)
        {
            if (this._data.Projects.Any(p => p.Id == item.Id))
            {
                this._data.Projects.Update(item);
            }
        }
    }
}
