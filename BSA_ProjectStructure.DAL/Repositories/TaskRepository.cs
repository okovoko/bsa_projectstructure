﻿using BSA_ProjectStructure.DAL.Context;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BSA_ProjectStructure.DAL.Repositories
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly TaskTrackerDbContext _data;

        public TaskRepository(TaskTrackerDbContext data)
        {
            this._data = data;
        }

        public void Create(Task item)
        {
            this._data.Tasks.Add(item);
        }

        public void Delete(int id)
        {
            if (this._data.Tasks.Any(t => t.Id == id))
            {
                this._data.Tasks.Remove(this._data.Tasks.Find(id));
            }
        }

        public Task Get(int id)
        {
            return this._data.Tasks.FirstOrDefault(t => t.Id == id);
        }

        public IEnumerable<Task> GetAll()
        {
            return this._data.Tasks;
        }

        public void Update(Task item)
        {
            if (this._data.Tasks.Any(t => t.Id == item.Id))
            {
                this._data.Tasks.Update(item);
            }
        }
    }
}
