﻿using BSA_ProjectStructure.DAL.Context;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA_ProjectStructure.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly TaskTrackerDbContext _data;

        public TeamRepository(TaskTrackerDbContext data)
        {
            this._data = data;
        }

        public void Create(Team item)
        {
            this._data.Teams.Add(item);
        }

        public void Delete(int id)
        {
            if (this._data.Teams.Any(t => t.Id == id))
            {
                this._data.Teams.Remove(this._data.Teams.Find(id));
            }
        }

        public Team Get(int id)
        {
            return this._data.Teams.FirstOrDefault(t => t.Id == id);
        }

        public IEnumerable<Team> GetAll()
        {
            return this._data.Teams;
        }

        public void Update(Team item)
        {
            if (this._data.Teams.Any(u => u.Id == item.Id))
            {
                this._data.Teams.Update(item);
            }
        }
    }
}
