﻿using BSA_ProjectStructure.DAL.Context;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;

namespace BSA_ProjectStructure.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private IRepository<Project> _projects;
        private IRepository<User> _users;
        private IRepository<Team> _teams;
        private IRepository<Task> _tasks;

        private readonly TaskTrackerDbContext _data;

        public UnitOfWork(TaskTrackerDbContext data)
        {
            this._data = data;
        }

        public IRepository<Project> Projects
        {
            get
            {
                if (this._projects == null)
                {
                    this._projects = new ProjectRepository(this._data);
                }
                return this._projects;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                if (this._users == null)
                {
                    this._users = new UserRepository(this._data);
                }
                return this._users;
            }
        }

        public IRepository<Team> Teams
        {
            get
            {
                if (this._teams == null)
                {
                    this._teams = new TeamRepository(this._data);
                }
                return this._teams;
            }
        }

        public IRepository<Task> Tasks
        {
            get
            {
                if (this._tasks == null)
                {
                    this._tasks = new TaskRepository(this._data);
                }
                return this._tasks;
            }
        }

        public void Save()
        {
            this._data.SaveChanges();
        }
    }
}
