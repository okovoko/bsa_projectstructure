﻿using BSA_ProjectStructure.DAL.Context;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BSA_ProjectStructure.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly TaskTrackerDbContext _data;

        public UserRepository(TaskTrackerDbContext data)
        {
            this._data = data;
        }

        public void Create(User item)
        {
            this._data.Users.Add(item);
        }

        public void Delete(int id)
        {
            if (this._data.Users.Any(u => u.Id == id))
            {
                this._data.Users.Remove(this._data.Users.Find(id));
            }
        }

        public User Get(int id)
        {
            return this._data.Users.FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<User> GetAll()
        {
            return this._data.Users;
        }

        public void Update(User item)
        {
            if (this._data.Users.Any(u => u.Id == item.Id))
            {
                this._data.Users.Update(item);
            }
        }
    }
}
