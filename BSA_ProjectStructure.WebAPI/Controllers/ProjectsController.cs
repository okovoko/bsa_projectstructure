﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BSA_projectStructure.BLL.DTOs;
using BSA_projectStructure.BLL.Interfaces;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BSA_ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IProjectService _projectService;

        public ProjectsController(IUnitOfWork unitOfWork, IMapper mapper, IProjectService projectService)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            this._projectService = projectService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetAll()
        {
            return Ok(this._unitOfWork.Projects.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<ProjectDTO>> Get(int id)
        {
            return Ok(this._unitOfWork.Projects.Get(id));
        }

        [HttpPost]
        public ActionResult Post([FromBody] ProjectDTO item)
        {
            this._unitOfWork.Projects.Create(this._mapper.Map<ProjectDTO, Project>(item));
            return Ok();
        }

        [HttpPut]
        public ActionResult Put([FromBody] UserDTO item)
        {
            this._unitOfWork.Projects.Update(this._mapper.Map<UserDTO, Project>(item));
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            this._unitOfWork.Projects.Delete(id);
            return Ok();
        }

        [Route("{id}/projectSummary")]
        [HttpGet]
        public ActionResult<ProjectSummaryInfoDTO> GetProjectSummaryInfo(int id)
        {
            return Ok(this._projectService.GetProjectSummaryInfo(id));
        }
    }
}
