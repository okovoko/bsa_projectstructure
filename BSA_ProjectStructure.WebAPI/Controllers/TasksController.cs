﻿using System.Collections.Generic;
using AutoMapper;
using BSA_projectStructure.BLL.DTOs;
using BSA_projectStructure.BLL.Interfaces;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BSA_ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ITasksService _tasksService;

        public TasksController(IUnitOfWork unitOfWork, IMapper mapper, ITasksService tasksService)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            this._tasksService = tasksService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> GetAll()
        {
            return Ok(this._unitOfWork.Tasks.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<TaskDTO>> Get(int id)
        {
            return Ok(this._unitOfWork.Tasks.Get(id));
        }

        [HttpPost]
        public ActionResult Post([FromBody] TaskDTO item)
        {
            this._unitOfWork.Tasks.Create(this._mapper.Map<TaskDTO, Task>(item));
            return Ok();
        }

        [HttpPut]
        public ActionResult Put([FromBody] TaskDTO item)
        {
            this._unitOfWork.Tasks.Update(this._mapper.Map<TaskDTO, Task>(item));
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            this._unitOfWork.Tasks.Delete(id);
            return Ok();
        }
    }
}
