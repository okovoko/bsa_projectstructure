﻿using System.Collections.Generic;
using AutoMapper;
using BSA_projectStructure.BLL.DTOs;
using BSA_projectStructure.BLL.Interfaces;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BSA_ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ITeamsService _teamsService;

        public TeamsController(IUnitOfWork unitOfWork, IMapper mapper, ITeamsService teamsService)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            this._teamsService = teamsService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> GetAll()
        {
            return Ok(this._unitOfWork.Teams.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<TeamDTO>> Get(int id)
        {
            return Ok(this._unitOfWork.Teams.Get(id));
        }

        [HttpPost]
        public ActionResult Post([FromBody] TeamDTO item)
        {
            this._unitOfWork.Teams.Create(this._mapper.Map<TeamDTO, Team>(item));
            return Ok();
        }

        [HttpPut]
        public ActionResult Put([FromBody] TeamDTO item)
        {
            this._unitOfWork.Teams.Update(this._mapper.Map<TeamDTO, Team>(item));
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            this._unitOfWork.Teams.Delete(id);
            return Ok();
        }

        [Route("ageLimitTeams")]
        [HttpGet]
        public ActionResult<IEnumerable<AgeLimitTeamDTO>> GetAgeLimitTeams()
        {
            return Ok(this._teamsService.GetAgeLimitTeams());
        }
    }
}
