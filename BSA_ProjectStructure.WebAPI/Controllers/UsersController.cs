﻿using System.Collections.Generic;
using AutoMapper;
using BSA_projectStructure.BLL.DTOs;
using BSA_projectStructure.BLL.Interfaces;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BSA_ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IUsersService _usersService;

        public UsersController(IUnitOfWork unitOfWork, IMapper mapper, IUsersService usersService)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            this._usersService = usersService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetAll()
        {
            return Ok(this._unitOfWork.Users.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<UserDTO>> Get(int id)
        {
            return Ok(this._unitOfWork.Users.Get(id));
        }

        [HttpPost]
        public ActionResult Post([FromBody] UserDTO item)
        {
            this._unitOfWork.Users.Create(this._mapper.Map<UserDTO, User>(item));
            return Ok();
        }

        [HttpPut]
        public ActionResult Put([FromBody] UserDTO item)
        {
            this._unitOfWork.Users.Update(this._mapper.Map<UserDTO, User>(item));
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            this._unitOfWork.Users.Delete(id);
            return Ok();
        }

        [Route("sortedUsers")]
        [HttpGet]
        public ActionResult<UserLastProjectInfoDTO> GetSortedUsers(int userId)
        {
            return Ok(this._usersService.GetSortedUsers());
        }

        [Route("{userId}/tasks")]
        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> GetUserTasks(int userId)
        {
            return Ok(this._usersService.GetUserTasks(userId));
        }

        [Route("{userId}/projectUserTasksCount")]
        [HttpGet]
        public ActionResult<IEnumerable<ProjectUserTasksCountDTO>> GetProjectUserTasksCount(int userId)
        {
            var data = this._usersService.GetProjectUserTasksCount(userId);
            return Ok(this._usersService.GetProjectUserTasksCount(userId));
        }

        [Route("{userId}/userFinihedTasks")]
        [HttpGet]
        public ActionResult<IEnumerable<UserFinihedTasksDTO>> GetFinishedTasks(int userId)
        {
            return Ok(this._usersService.GetFinishedTasks(userId));
        }

        [Route("{userId}/lastProject")]
        [HttpGet]
        public ActionResult<UserLastProjectInfoDTO> GetUserLastProjectInfo(int userId)
        {
            return Ok(this._usersService.GetUserLastProjectInfo(userId));
        }
    }
}
