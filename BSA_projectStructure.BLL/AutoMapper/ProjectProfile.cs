﻿using AutoMapper;
using BSA_projectStructure.BLL.DTOs;
using BSA_ProjectStructure.DAL.Etities;

namespace BSA_projectStructure.BLL.AutoMapper
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDTO, Project>();
            CreateMap<Project, ProjectDTO>();
        }
    }
}
