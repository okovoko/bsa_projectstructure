﻿using AutoMapper;
using BSA_projectStructure.BLL.DTOs;
using BSA_ProjectStructure.DAL.Etities;

namespace BSA_projectStructure.BLL.AutoMapper
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TaskDTO, Task>();
            CreateMap<Task, TaskDTO>();
        }
    }
}
