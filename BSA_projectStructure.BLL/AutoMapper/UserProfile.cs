﻿using AutoMapper;
using BSA_projectStructure.BLL.DTOs;
using BSA_ProjectStructure.DAL.Etities;

namespace BSA_projectStructure.BLL.AutoMapper
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDTO, User>();
            CreateMap<User, UserDTO>();
        }
    }
}
