﻿using BSA_ProjectStructure.DAL.Etities;
using System.Collections.Generic;

namespace BSA_projectStructure.BLL.DTOs
{
    public class AgeLimitTeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDTO> Users { get; set; }
    }
}
