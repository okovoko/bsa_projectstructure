﻿using System;

namespace BSA_projectStructure.BLL.DTOs
{
    public class ProjectDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }

        public int AuthorId { get; set; }
        public int? TeamId { get; set; }
    }
}
