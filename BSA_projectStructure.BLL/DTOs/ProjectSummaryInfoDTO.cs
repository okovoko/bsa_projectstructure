﻿using BSA_ProjectStructure.DAL.Etities;

namespace BSA_projectStructure.BLL.DTOs
{
    public class ProjectSummaryInfoDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestDescriptionTask { get; set; }
        public TaskDTO ShortestNameTask { get; set; }
        public int UserCount { get; set; }
    }
}
