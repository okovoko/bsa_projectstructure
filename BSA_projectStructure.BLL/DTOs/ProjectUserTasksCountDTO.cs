﻿namespace BSA_projectStructure.BLL.DTOs
{
    public class ProjectUserTasksCountDTO
    {
        public ProjectDTO Project { get; set; }
        public int UserTasksCount { get; set; }
    }
}
