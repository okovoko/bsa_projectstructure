﻿using BSA_ProjectStructure.DAL.Etities;
using System.Collections.Generic;

namespace BSA_projectStructure.BLL.DTOs
{
    public class SortedUsersDTO
    {
        public UserDTO User { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}
