﻿using System;

namespace BSA_projectStructure.BLL.DTOs
{
    public class TeamDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
