﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSA_projectStructure.BLL.DTOs
{
    public class UserFinihedTasksDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
