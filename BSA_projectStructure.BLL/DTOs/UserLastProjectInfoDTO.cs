﻿using BSA_ProjectStructure.DAL.Etities;

namespace BSA_projectStructure.BLL.DTOs
{
    public class UserLastProjectInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO Project { get; set; }
        public int TasksCount { get; set; }
        public int UnfinishedTasksCount { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
