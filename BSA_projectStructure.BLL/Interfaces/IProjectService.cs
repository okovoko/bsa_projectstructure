﻿using BSA_projectStructure.BLL.DTOs;

namespace BSA_projectStructure.BLL.Interfaces
{
    public interface IProjectService
    {
        ProjectSummaryInfoDTO GetProjectSummaryInfo(int projectId);
    }
}
