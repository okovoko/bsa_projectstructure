﻿using BSA_projectStructure.BLL.DTOs;
using System.Collections.Generic;

namespace BSA_projectStructure.BLL.Interfaces
{
    public interface ITeamsService
    {
        IEnumerable<AgeLimitTeamDTO> GetAgeLimitTeams(int ageLimitYears = 12);
    }
}
