﻿using BSA_projectStructure.BLL.DTOs;
using System.Collections.Generic;

namespace BSA_projectStructure.BLL.Interfaces
{
    public interface IUsersService
    {
        IEnumerable<SortedUsersDTO> GetSortedUsers();
        IEnumerable<TaskDTO> GetUserTasks(int userId, int maxTaskNameLength = 45);
        IEnumerable<ProjectUserTasksCountDTO> GetProjectUserTasksCount(int userId);
        IEnumerable<UserFinihedTasksDTO> GetFinishedTasks(int userId, int finishYear = 2019);
        UserLastProjectInfoDTO GetUserLastProjectInfo(int userId);
    }
}
