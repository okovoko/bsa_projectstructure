﻿using BSA_projectStructure.BLL.DTOs;
using BSA_projectStructure.BLL.Interfaces;
using BSA_ProjectStructure.Common;
using BSA_ProjectStructure.DAL.Interfaces;
using System.Linq;

namespace BSA_projectStructure.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProjectService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public ProjectSummaryInfoDTO GetProjectSummaryInfo(int projectId)
        {
            return (from project in this._unitOfWork.Projects.GetAll()
                    where project.Id == projectId
                    join task in this._unitOfWork.Tasks.GetAll() on project.Id equals task.ProjectId
                    into tasksList
                    let longestDescriptionTask = (from task in this._unitOfWork.Tasks.GetAll()
                                                  where task.ProjectId == projectId
                                                  orderby task.Description descending
                                                  select task).FirstOrDefault()
                    let shortestNameTask = (from task in this._unitOfWork.Tasks.GetAll()
                                            where task.ProjectId == projectId
                                            orderby task.Name
                                            select task).FirstOrDefault()
                    let users = from user in this._unitOfWork.Users.GetAll()
                                where user.UserTeams.Where(ut => ut.TeamId == project.TeamId).Select(ut => ut.TeamId).FirstOrDefault() == project.TeamId
                                    && (project.Description.Length > 25
                                    || tasksList.Count() < 3)
                                select user
                    select new ProjectSummaryInfoDTO()
                    {
                        Project = new ProjectDTO()
                        {
                            Id = project.Id,
                            Name = project.Name,
                            Description = project.Description,
                            Deadline = project.Deadline,
                            CreatedAt = project.CreatedAt,
                            AuthorId = project.AuthorId,
                            TeamId = project.TeamId
                        },
                        LongestDescriptionTask = new TaskDTO()
                        {
                            Id = longestDescriptionTask.Id,
                            Name = longestDescriptionTask.Name,
                            Description = longestDescriptionTask.Description,
                            CreatedAt = longestDescriptionTask.CreatedAt,
                            FinishedAt = longestDescriptionTask.FinishedAt,
                            StateId = longestDescriptionTask.StateId,
                            ProjectId = longestDescriptionTask.ProjectId,
                            PerformerId = longestDescriptionTask.PerformerId
                        },
                        ShortestNameTask = new TaskDTO()
                        {
                            Id = shortestNameTask.Id,
                            Name = shortestNameTask.Name,
                            Description = shortestNameTask.Description,
                            CreatedAt = shortestNameTask.CreatedAt,
                            FinishedAt = shortestNameTask.FinishedAt,
                            StateId = shortestNameTask.StateId,
                            ProjectId = shortestNameTask.ProjectId,
                            PerformerId = shortestNameTask.PerformerId
                        },
                        UserCount = users.Count()
                    }).FirstOrDefault();
        }
    }
}
