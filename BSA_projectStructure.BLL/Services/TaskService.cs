﻿using BSA_projectStructure.BLL.DTOs;
using BSA_projectStructure.BLL.Interfaces;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BSA_projectStructure.BLL.Services
{
    public class TaskService : ITasksService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TaskService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
    }
}
