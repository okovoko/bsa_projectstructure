﻿using BSA_projectStructure.BLL.DTOs;
using BSA_projectStructure.BLL.Interfaces;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSA_projectStructure.BLL.Services
{
    public class TeamService : ITeamsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TeamService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<AgeLimitTeamDTO> GetAgeLimitTeams(int ageLimitYears = 12)
        {
            return this._unitOfWork.Users.GetAll()
                .Where(u => u.Birthday.Year > DateTime.Now.Year - ageLimitYears)
                .OrderByDescending(u => u.RegisteredAt)
                .GroupBy(u => u.UserTeams.Select(ut => ut.TeamId).FirstOrDefault())
                .Join(this._unitOfWork.Teams.GetAll(), u => u.Key, t => t.Id, (u, t) =>
                new AgeLimitTeamDTO()
                {
                    Id = t.Id,
                    Name = t.Name,
                    Users = u.Select(user => new UserDTO()
                    {
                        Id = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email,
                        Birthday = user.Birthday,
                        RegisteredAt = user.RegisteredAt,
                        TeamId = user.UserTeams.Where(ut => ut.UserId == user.Id).Select(ut => ut.TeamId).FirstOrDefault()
                    }).ToList<UserDTO>()
                });
        }
    }
}
