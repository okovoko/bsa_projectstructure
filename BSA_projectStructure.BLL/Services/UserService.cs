﻿using BSA_projectStructure.BLL.DTOs;
using BSA_projectStructure.BLL.Interfaces;
using BSA_ProjectStructure.Common;
using BSA_ProjectStructure.DAL.Etities;
using BSA_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BSA_projectStructure.BLL.Services
{
    public class UserService : IUsersService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<SortedUsersDTO> GetSortedUsers()
        {
            return this._unitOfWork.Users.GetAll()
                .OrderBy(u => u.FirstName)
                .Select((u) => new SortedUsersDTO()
                {
                    User = new UserDTO()
                    {
                        Id = u.Id,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        Email = u.Email,
                        Birthday = u.Birthday,
                        RegisteredAt = u.RegisteredAt,
                        TeamId = u.UserTeams.Where(ut => ut.UserId == u.Id).Select(ut => ut.TeamId).FirstOrDefault()
                    },
                    Tasks = this._unitOfWork.Tasks.GetAll().Where(t => t.PerformerId == u.Id)
                            .OrderByDescending(t => t.Name.Length).Select(t => new TaskDTO()
                            {
                                Id = t.Id,
                                Name = t.Name,
                                Description = t.Description,
                                CreatedAt = t.CreatedAt,
                                FinishedAt = t.FinishedAt,
                                StateId = t.StateId,
                                ProjectId = t.ProjectId,
                                PerformerId = t.PerformerId
                            }).ToList<TaskDTO>()
                });
        }

        public IEnumerable<TaskDTO> GetUserTasks(int userId, int maxTaskNameLength = 45)
        {
            return this._unitOfWork.Tasks.GetAll()
                        .Where(t => t.PerformerId == userId && t.Name.Length < maxTaskNameLength)
                        .Select(t => new TaskDTO()
                        {
                            Id = t.Id,
                            Name = t.Name
                        });
        }

        public IEnumerable<ProjectUserTasksCountDTO> GetProjectUserTasksCount(int userId)
        {
            return from t in this._unitOfWork.Tasks.GetAll()
                   group t by t.ProjectId into tp
                   join p in this._unitOfWork.Projects.GetAll() on tp.Key equals p.Id
                   where p.AuthorId == userId
                   select new ProjectUserTasksCountDTO
                   {
                       Project = new ProjectDTO()
                       {
                           Id = p.Id,
                           Name = p.Name,
                           Description = p.Description,
                           Deadline = p.Deadline,
                           CreatedAt = p.CreatedAt,
                           AuthorId = p.AuthorId,
                           TeamId = p.TeamId
                       },
                       UserTasksCount = tp.Count()
                   };
        }

        public IEnumerable<UserFinihedTasksDTO> GetFinishedTasks(int userId, int finishYear = 2019)
        {
            return this._unitOfWork.Tasks.GetAll()
                .Where(t => t.PerformerId == userId)
                .Where(t => t.FinishedAt.Year == finishYear)
                .Select(t => new UserFinihedTasksDTO
                {
                    Id = t.Id,
                    Name = t.Name
                });
        }

        public UserLastProjectInfoDTO GetUserLastProjectInfo(int userId)
        {
            return (from project in this._unitOfWork.Projects.GetAll()
                    where project.AuthorId == userId
                    orderby project.CreatedAt descending
                    let user = this._unitOfWork.Users.GetAll().Where(x => x.Id == userId).FirstOrDefault()
                    let lastProjectTasksCount = this._unitOfWork.Tasks.GetAll().GroupBy(x => x.ProjectId)
                                                               .Where(x => x.Key == project.Id)
                                                               .FirstOrDefault().Count()
                    let unfinishedTasks = this._unitOfWork.Tasks.GetAll().Count(x => x.ProjectId == project.Id && x.StateId != (int)TaskStates.Finished)
                    let longestTask = this._unitOfWork.Tasks.GetAll().Where(x => x.PerformerId == user.Id)
                                          .OrderBy(x => x.CreatedAt)
                                          .ThenByDescending(x => x.FinishedAt).FirstOrDefault()
                    select new UserLastProjectInfoDTO()
                    {
                        User = new UserDTO()
                        {
                            Id = user.Id,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Email = user.Email,
                            Birthday = user.Birthday,
                            RegisteredAt = user.RegisteredAt,
                            TeamId = user.UserTeams.Where(ut => ut.UserId == user.Id).Select(ut => ut.TeamId).FirstOrDefault()
                        },
                        Project = new ProjectDTO()
                        {
                            Id = project.Id,
                            Name = project.Name,
                            Description = project.Description,
                            Deadline = project.Deadline,
                            CreatedAt = project.CreatedAt,
                            AuthorId = project.AuthorId,
                            TeamId = project.TeamId
                        },
                        TasksCount = lastProjectTasksCount,
                        UnfinishedTasksCount = unfinishedTasks,
                        LongestTask = new TaskDTO()
                        {
                            Id = longestTask.Id,
                            Name = longestTask.Name,
                            Description = longestTask.Description,
                            CreatedAt = longestTask.CreatedAt,
                            FinishedAt = longestTask.FinishedAt,
                            StateId = longestTask.StateId,
                            ProjectId = longestTask.ProjectId,
                            PerformerId = longestTask.PerformerId
                        }
                    }).FirstOrDefault();
        }
    }
}
